<?php get_header(); ?>

<?php $grve_blog_style = grve_option( 'blog_style', 'large-media' ); ?>

<section class="grve-section">
	<div class="grve-container">
		<!-- Content -->
		<div class="grve-main-content">
			<div class="grve-element grve-search-page">

				<?php
					if ( have_posts() ) :
						$grve_post_items = $grve_page_items = $grve_portfolio_items = $grve_other_post_items = 0;
						$grve_has_post_items = $grve_has_page_items = $grve_has_portfolio_items = 0;

						while ( have_posts() ) : the_post();
							$post_type = get_post_type();
							switch( $post_type ) {
								case 'post':
									 $grve_post_items++;
									 $grve_has_post_items = 1;
								break;
								case 'page':
									 $grve_page_items++;
									 $grve_has_page_items = 1;
								break;
								case 'portfolio':
									 $grve_portfolio_items++;
									 $grve_has_portfolio_items = 1;
								break;
								default:
									$grve_other_post_items++;
								break;
							}
						endwhile;

						$grve_item_types = $grve_has_post_items + $grve_has_page_items + $grve_has_portfolio_items;

						if ( $grve_item_types > 1 ) {
				?>
							<!-- Search Filter -->
							<div class="grve-filter">
								<ul>
									<li data-filter="*" class="selected"><?php _e( "All", GRVE_THEME_TRANSLATE ); ?></li>
									<?php if ( $grve_has_post_items ) { ?>
									<li data-filter=".post"><?php _e( "Post", GRVE_THEME_TRANSLATE ); ?></li>
									<?php } ?>
									<?php if ( $grve_has_page_items ) { ?>
									<li data-filter=".page"><?php _e( "Page", GRVE_THEME_TRANSLATE ); ?></li>
									<?php } ?>
									<?php if ( $grve_has_portfolio_items ) { ?>
									<li data-filter=".portfolio"><?php _e( "Portfolio", GRVE_THEME_TRANSLATE ); ?></li>
									<?php } ?>
								</ul>
							</div>
				<?php
						}
				?>
				<div class="grve-isotope-container" data-columns="3" data-layout="masonry">
				<?php

						// Start the Loop.
						while ( have_posts() ) : the_post();
							//Get post template
							get_template_part( 'templates/search', 'masonry' );

						endwhile;
				?>
				</div>
				<?php
						// Previous/next post navigation.
						grve_pagination();
					else :
						// If no content, include the "No posts found" template.
						get_template_part( 'content', 'none' );
					endif;
				?>

			</div>
		</div>
		<!-- End Content -->

	</div>
</section>
<?php get_footer(); ?>