<?php
/**
 * JT[12.04.16] Complete change of search-result template based on "tag.php"
 */

get_header();
?>

<section class="grve-section grve-blog-masonry">
	<div class="grve-container <?php echo grve_sidebar_class(); ?>">
		<!-- Content -->
		<div class="grve-main-content">

			<h1><?php /* Search Count */

			// DL:
			// ich weiß nicht, was `&new` bedeutet,
			// aber ohne `&` funktioniert's bei mir.
			// ggf. zurück ändern

			// $allsearch = &new WP_Query("s=$s&showposts=-1");

			$allsearch = new WP_Query("s=$s&showposts=-1");

			$key = wp_specialchars($s, 1);
			$count = $allsearch->post_count; _e('');

			// // DL:
			// // _e(' &mdash; '); // brauchen wir nicht.
			echo $count . ' ';
			_e('Treffer ');
			_e('für ');
			_e('<span class="search-terms">');
			echo $key; _e('</span>');
			wp_reset_query();

			?></h1>

			<!-- sk-teaser-list -->
				<?php
				$args = array(
					's' => $s,
					'showposts' => 4,
					'paged' => get_query_var('paged')
				);
				query_posts( $args );
				require( locate_template( 'template-parts/content-teaserlist.php' ) );
				wp_reset_query();
				?>

		</div>
		<!-- End Content -->

		<?php get_sidebar(); ?>

	</div>
</section>
<?php get_footer(); ?>
