<?php

$bilder = [];

// 1. lead image (post thumbnail)
$thumbnail_id = get_post_thumbnail_id();

if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  $imagepost = get_post( $thumbnail_id );
  $bilder[] = [
    'url'       => get_the_post_thumbnail_url(null, 'guest-author-128'),
    'copyright' => $imagepost->post_content
  ];
}

// 2. content images
$content_images = get_posts( [
  'post_type'      => 'attachment',
  'post_mime_type' => 'image',
  'numberposts'    => -1,
  'post_parent'    => $post->ID,
  'orderby'        => 'menu_order',
  'order'          => 'asc',
  'post__not_in'   => [ $thumbnail_id ]
] );

foreach($content_images as $image) {
  $bilder[] = [
    'url'          => wp_get_attachment_image_src($image->ID, 'guest-author-128')[0],
    'copyright'    => $image->post_content
  ];
}

// 3. author portraits
$authors = get_coauthors($post->ID);
foreach( $authors as $author ) {

  $copyright = get_post_meta($author->ID, 'cap-aim', true);

  $bilder[] = [
    'url'         => get_the_post_thumbnail_url( $author->ID, 'guest-author-128' ),
    'copyright'   => $copyright ? $copyright : '© ' . $author->display_name
  ];
}

// remove images /wo copyrights
$bilder = array_filter($bilder, function($item) {
  return !(!$item['copyright']);
});

?>
<div class="sk-copyrights">
  <p><a href="javascript:void()" class="btn-visibility-copyrights">Bildrechte</a></p>
  <ul class="list-image-with-text clearfix">
    <?php foreach($bilder as $bild) : ?>
      <li>
        <figure><img src="<?=$bild['url'];?>" /></figure>
        <figcaption><?=$bild['copyright'];?></figcaption>
      </li>
    <?php endforeach; ?>
  </ul>
</div>