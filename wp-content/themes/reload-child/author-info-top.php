<!-- About Author -->
<?php if( function_exists( 'get_coauthors' ) ): ?>
 	<?php $coauthors = get_coauthors($post->ID);?>
<?php endif;?>

<?php if( $coauthors ):?>
		<div class="grve-author-info">
			<div class="sk-author-title">
				<?php $i = 0; ?>
				<?php foreach( $coauthors as $coauthor ): ?>
					<?php if ($i > 0): ?> 
						<?php echo ' & '?> 
					<?php endif;?>
					<?php $i++; ?>
					<?php echo $coauthor->display_name . ' '?>
				<?php endforeach; ?>
			</div>
		</div>
<?php else:?>
	<?php echo get_avatar( get_the_author_meta('ID'), 80 ); ?>
	<div class="grve-author-info">
		<div class="sk-author-title"><?php echo __( 'About', GRVE_THEME_TRANSLATE ) . '  '; ?>
			<?php if ( function_exists( 'coauthors_posts_links' ) ) { coauthors_posts_links(); } else { the_author_posts_link(); } ?>
		</div>
		<?php echo get_the_author_meta( 'user_description' ); ?>
	</div>
<?php endif;?>
<!-- End About Author -->