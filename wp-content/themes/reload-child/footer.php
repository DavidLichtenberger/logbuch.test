


			</div> <!-- end #grve-theme-body -->
		</div> <!-- end #grve-wrapper -->

		<footer id="grve-footer" class="grve-section">
			<?php grve_print_footer_widgets(); ?>
			<?php grve_print_footer_copyright(); ?>
		</footer>

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>
</html>