<?php get_header(); ?>

<div class="grve-section">
	<div class="grve-container <?php echo grve_sidebar_class(); ?>">
		<!-- Content -->
		<div class="grve-main-content">
			<!-- Blog FitRows -->
			<div class="grve-element grve-blog ">

				<!-- sk-teaser-list -->
				<?php
				$args = array(
					'posts_per_page' => 6,
					'paged' => get_query_var('paged')
				);
				query_posts( $args );
				require( locate_template( 'template-parts/content-teaserlist.php' ) );
				wp_reset_query();
				?>

			</div>
			<!-- End Element Blog -->
		</div>
		<!-- End Content -->

		<?php get_sidebar(); ?>

	</div>
</div>



<?php /* JT 7.11.17
<?php $grve_blog_style = grve_option( 'blog_style', 'large-media' ); ?>

<div class="grve-section">
	<div class="grve-container <?php echo grve_sidebar_class(); ?>">
		<!-- Content -->
		<div class="grve-main-content">
			<!-- Blog FitRows -->
			<div class="grve-element grve-blog <?php grve_print_blog_class( $grve_blog_style ); ?>">
				<?php
				if ( have_posts() ) :
					if ( 'labeled' == $grve_blog_style ) {
						grve_print_blog_labeled_filter();
					}

				if ( 'large-media' != $grve_blog_style ) {
				?>
				<div class="grve-isotope-container" <?php grve_print_blog_data( $grve_blog_style ); ?>>
				<?php
				} else {
				?>
				<div class="grve-standard-container">

				<?php
				}

				// Start the Loop.
				while ( have_posts() ) : the_post();
					//Get post template
					get_template_part( 'content', get_post_format() );

				endwhile;

				?>
				</div>
				<?php
					// Previous/next post navigation.
					grve_pagination();
				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );
				endif;
				?>

				<!-- End Blog FitRows -->
			</div>
			<!-- End Element Blog -->
		</div>
		<!-- End Content -->

		<?php get_sidebar(); ?>

	</div>
</div>

*/ ?>
<?php get_footer(); ?>