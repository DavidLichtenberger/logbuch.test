<?php
/**
 * The default post template
 */
?>
<?php
if ( is_singular() ) {
	//JT[10.04.2016 $grve_disable_media = grve_post_meta( 'grve_disable_media' );
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('grve-single-post'); ?>>
		<?php
			//JT[10.04.2016
			/* old
			if ( '1' == grve_option( 'post_feature_visibility', '1' ) ) {
				grve_print_post_feature_media();
			}
			new: */
			if ( '1' == grve_option( 'post_feature_visibility', '1' ) && 'yes' != $grve_disable_media && !post_password_required() ) {
				grve_print_post_feature_media();
			}
		?>
		<div class="grve-post-content">
			<?php the_content(); ?>
		</div>

		<?= get_field('fussnoten_feld') ? get_field('fussnoten_feld') : '';?>
	</article>

<?php
} else {
	$grve_blog_style = grve_option( 'blog_style', 'large-media' );
	if ( 'labeled' != $grve_blog_style ) {
		$grve_blog_class = 'grve-standard-item';
		if ( 'large-media' != $grve_blog_style ) {
			$grve_blog_class = 'grve-isotope-item';
		}
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( $grve_blog_class ); ?>>
		<?php grve_print_post_feature_media( $grve_blog_style ); ?>
		<div class="grve-post-content">
			<?php grve_print_post_author( $grve_blog_style ); ?>

			<?php the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><h5 class="grve-post-title">', '</h5></a>' ); ?>

			<div class="grve-post-meta">
				<?php grve_print_post_date(); ?>
				<?php grve_print_post_comments( $grve_blog_style ); ?>
				<?php grve_print_post_categories( $grve_blog_style ); ?>
				<?php grve_print_post_author_by( $grve_blog_style ); ?>
			</div>

			<div class="sk-post-author">
				<?php if( function_exists( 'get_coauthors' ) ): ?>
				 	<?php $coauthors = get_coauthors($post->ID);?>
				 	<?php // var_dump($coauthors)?>
				<?php endif;?>
			</div>

			<?php grve_print_post_excerpt( $grve_blog_style, 'standard' ); ?>
		</div>
	</article>

<?php
	} else {
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'grve-isotope-item' ); ?>>
		<a href="<?php echo esc_url( get_permalink() ); ?>" class="grve-post-wraper">
			<div class="grve-post-format"></div>
			<div class="grve-post-content">
				<?php grve_print_post_date(); ?>
				<div class="grve-post-title"><?php the_title(); ?></div>
			</div>
		</a>
	</article>
<?php
	}
}
?>



