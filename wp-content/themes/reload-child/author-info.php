<!-- About Author -->
<?php /* JT 27.11 replaced
<?php if( function_exists( 'get_coauthors' ) ): ?>
 	<?php $coauthors = get_coauthors($post->ID);?>
<?php endif;?>

<?php if( $coauthors ):?>
	<?php foreach( $coauthors as $coauthor ): ?>
		<div class="sk-author-image">
			<a href="<?php bloginfo('url')?>/autor/<?php echo $coauthor->user_login?>">
				<?php echo get_the_post_thumbnail( $coauthor->ID, 'guest-author-96' ); ?>
			</a>
		</div>
		<div class="grve-author-info">
			<h2 class="sk-author-title">
				<a href="<?php bloginfo('url')?>/autor/<?php echo $coauthor->user_login?>"><?php echo $coauthor->display_name?></a>
			</h2>

			<div class="sk-author-description"><?php echo do_shortcode( $coauthor->description );?></div>

			<?php $cap_yahooim = get_post_meta($coauthor->ID, 'cap-yahooim', true)?>
			<?php if (!empty($cap_yahooim)):?>
			<a class="author-link-small" href="<?php echo $cap_yahooim;?>" target="_blank" rel="nofollow">
				<?php _e('Website von')?> <?php echo $coauthor->display_name?>
			</a>
			<br/>
			<?php endif;?>

			<?php $cap_website = get_post_meta($coauthor->ID, 'cap-website', true)?>
			<?php if (!empty($cap_website)): ?>
			<a class="author-link-small" href="<?php echo $cap_website ?>" target="_blank" rel="nofollow">
				<?php echo $coauthor->display_name?> <?php _e('bei Suhrkamp')?>
			</a>
			<?php endif;?>

			<p class="sk-author-copyright-info"> Foto: <?php echo get_post_meta($coauthor->ID, 'cap-aim', true); ?></p>



		</div>
		<div class="sk-clear"></div>
	<?php endforeach?>
<?php else:?>
	<?php echo get_avatar( get_the_author_meta('ID'), 80 ); ?>
	<div class="grve-author-info">
		<h2 class="sk-author-title"><?php echo __( 'About', GRVE_THEME_TRANSLATE ) . '  '; ?>
			<?php if ( function_exists( 'coauthors_posts_links' ) ) { coauthors_posts_links(); } else { the_author_posts_link(); } ?>
		</h2>
		<?php echo get_the_author_meta( 'user_description' ); ?>
	</div>
<?php endif;?>
<!-- End About Author -->
*/ ?>
<?php if( function_exists( 'get_coauthors' ) ): ?>
 	<?php $coauthors = get_coauthors($post->ID);?>
<?php endif;?>

<?php if( $coauthors ):?>
	<?php foreach( $coauthors as $coauthor ): ?>
		<div class="sk-author-image">
			<?php echo get_the_post_thumbnail( $coauthor->ID, 'guest-author-128' ); ?>
		</div>
		<div class="grve-author-info">
			<h2 class="sk-author-title">
				<?php echo $coauthor->display_name?>
			</h2>
			<a href="<?php bloginfo('url')?>/autor/<?php echo $coauthor->user_login?>">Zur Person</a>
		</div>
	<?php endforeach?>
<?php else:?>
	<?php echo get_avatar( get_the_author_meta('ID'), 80 ); ?>
	<div class="grve-author-info">
		<h2 class="sk-author-title"><?php echo __( 'About', GRVE_THEME_TRANSLATE ) . '  '; ?>
			<?php if ( function_exists( 'coauthors_posts_links' ) ) { coauthors_posts_links(); } else { the_author_posts_link(); } ?>
		</h2>
		<?php // echo get_the_author_meta( 'user_description' ); ?>
	</div>
<?php endif;?>
<!-- End About Author -->