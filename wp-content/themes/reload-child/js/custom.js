// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};


( function( $ ) {

	/* Vars */
	
	var wh; //windowheight
	var ww; //windowwidth
	var wr; //windowratio
		
    $( document ).ready(function() { 
    	
	  	/* Dimensions & Resizing */

	  		var menubtn = $('.grve-menu-btn');

	    	//adjust dimensions   
		    var adjustDimensions = function() {

    	        // Get window dim
    				wh = window.innerHeight;
    				ww = window.innerWidth;
    				wr = ww/wh;
    
    			if( ww <= 767 ){
    				$('.header-social-media').appendTo('#menu-kategorien').css("opacity",1);
    				
    			} else {

    				$('.header-social-media').insertBefore('.sk-mobile-menu').css("opacity",1).css("display","block");

    			}	

			};

			var resize = debounce(function() {
		    	adjustDimensions();
			}, 50);
			window.addEventListener('resize', resize);    		
			adjustDimensions();	 

    });

} )( jQuery ); 