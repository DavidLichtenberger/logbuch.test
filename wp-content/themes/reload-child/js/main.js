//JT 20.11.17 // var grveResponsiveThreshold = 960;
var grveResponsiveThreshold = 767;


(function( $ ) {

	'use strict';

	// -------------------------------------------------------------------------------------------
	// FEATURE SECTION
	// -------------------------------------------------------------------------------------------
	var featureSection = {

		init: function(){
			featureSection.config = {
				$container: $( '#grve-feature-section' )
			};

			var section = featureSection.config.$container;
			if(verticalMenu) {
				section.css( 'position','fixed' );
			}
			if( section.hasClass( 'grve-fullscreen' ) ){
				featureSection.fullScreen();
				$(window).smartresize( function() {
					featureSection.fullScreen();
				});
			} else {
				featureSection.customSize();
			}

			if(isMobile.any()) {
				featureSection.mobileFixes();
			}

			featureSection.featureImageLoad();

		},

		fullScreen: function(settings){

			if(isMobile.any()) {
				if( window.location !== window.parent.location ) {
					featureSection.customSize({customHeight : '500'});
					featureSection.config.$container.removeClass('grve-fullscreen');
				}
			}
			featureSection.fullScreen.config = {
				initHeight : parseInt( $('#grve-header').attr('data-height') )
			};
			// allow overriding the default config
			$.extend( featureSection.fullScreen.config, settings );

			var windowHeight  = $(window).height(),
				headerHeight  = featureSection.fullScreen.config.initHeight,
				section       = featureSection.config.$container,
				wpBarHeight   = $( 'body' ).hasClass( 'admin-bar' ) ? 32 : 0,
				sectionHeight = windowHeight - headerHeight - wpBarHeight;
			if(isMobile.any()) {
				windowHeight  = window.innerHeight;
			}
			section.css({ 'height' : sectionHeight });
			section.parent().css({ 'height' : sectionHeight });
			if( section.find('.grve-slider').length ) {
				section.find('.grve-slider li').css({ 'height' : sectionHeight });
			}
			if( section.find('.grve-map').length ) {
				section.find('.grve-map').css({ 'height' : sectionHeight });
			}

		},

		customSize: function(settings){
			this.customSize.config = {
				customHeight: this.config.$container.attr('data-height'),
			};

			// allow overriding the default config
			$.extend( this.customSize.config, settings );

			var section       = featureSection.config.$container,
				sectionHeight = parseInt(this.customSize.config.customHeight);

			if( section.parent().attr('data-responsive') == 'no') {
				section.css({ 'height' : sectionHeight });
				section.parent().css({ 'height' : sectionHeight });
				if( section.find( '.grve-slider').length ) {
					section.find( '.grve-slider li').css({ 'height' : sectionHeight });
				}
				if( section.find( '.grve-map').length ) {
					section.find( '.grve-map').css({ 'height' : sectionHeight });
				}
				return;
			}

			featureSection.resizeFeature( section, sectionHeight );
			$(window).smartresize( function() {
				featureSection.resizeFeature( section, sectionHeight );
			});
		},

		resizeFeature: function( section, sectionHeight ){

			if( $(window).width() >= 1200 ) {
				section.css({ 'height' : sectionHeight });
				section.parent().css({ 'height' : sectionHeight });
				if( section.find('.grve-slider').length ) {
					section.find('.grve-slider').css({ 'height' : sectionHeight });
				}
				if( section.find('.grve-map').length ) {
					section.find('.grve-map').css({ 'height' : sectionHeight });
				}
			} else {
				var ratio = sectionHeight / 1200,
					resizeHeight = $(window).width() * ratio;

				section.css({ 'height' : resizeHeight });
				section.parent().css({ 'height' : resizeHeight });
				if( section.find('.grve-slider').length ) {
					section.find('.grve-slider').css({ 'height' : resizeHeight });
				}
				if( section.find('.grve-map').length ) {
					section.find('.grve-map').css({ 'height' : resizeHeight });
				}
			}
		},

		mobileFixes: function(){
			featureSection.config.$container.css({ 'position':'relative' });
		},

		featureImageLoad: function(){

			var $bgImage     = featureSection.config.$container.find('.grve-bg-image'),
				totalBgImage = $bgImage.length;

			if (!totalBgImage) {
				return;
			}

			var waitImgDone = function() {
				totalBgImage--;
				if (!totalBgImage) {

					if( featureSection.config.$container.find( '.grve-slider').length ) {
						featureSlider.init();
					} else {
						$bgImage.delay(200).animate({'opacity':1},900);
					}
				}
			};

			$bgImage.each(function () {
				function imageUrl(input) {
					return input.replace(/"/g,"").replace(/url\(|\)$/ig, "");
				}
				var image = new Image(),
					$that = $(this);
				image.src = imageUrl($that.css('background-image'));
				$(image).load(waitImgDone).error(waitImgDone);
			});
		}
	};

	// -------------------------------------------------------------------------------------------
	// FEATURE SLIDER
	// -------------------------------------------------------------------------------------------
	var featureSlider = {

		init: function(){

			var $slider     = $( '.grve-feature-element.grve-slider' ),
				sliderSpeed = parseInt( $slider.attr('data-slider-speed') ) ? parseInt( $slider.attr('data-slider-speed') ) : 3500,
				sliderNav   = $slider.attr('data-slider-dirnav') == 'no' ? false : true;

			$slider.flexslider({
				animation: 'fade',
				controlNav: false,
				smoothHeight: false,
				animationSpeed: 900,
				easing: 'linear',
				nextText: ' ',
				prevText: ' ',
				directionNav: sliderNav,
				pauseOnHover: false,
				animationLoop: true,
				useCSS: false,
				slideshowSpeed: sliderSpeed,
				start: function(){
					$slider.find('.grve-bg-image').css({'opacity':1});
					$slider.find('.grve-slider-caption').delay(400).animate({'opacity':1},600);
				}
			});

		}
	};

	// -------------------------------------------------------------------------------------------
	// HEADER SETTINGS
	// -------------------------------------------------------------------------------------------
	var headerSettings = {

		init: function(){

			var $imgLogo = $('#grve-header .grve-logo img'),
				$header  = $('#grve-header');

			$header.wrap( '<div id="grve-header-wrapper"></div>' );

			if(!verticalMenu) {
				$header.addClass('sticky');
				headerSettings.showTitle();
				headerSettings.sickyHeader();
				$('.grve-page-title').addClass('grve-horizontal-title');
				$(window).smartresize( function() {
					headerSettings.sickyHeader();
				});
				return;
			}

			headerSettings.logoSize($imgLogo);

		},
		logoSize: function(logo){
			imagesLoaded(logo,function(){
				var logoWidth = logo.outerWidth() + 60;

				headerSettings.pageTitleSize(logoWidth);
				$(window).smartresize( function() {
					headerSettings.pageTitleSize(logoWidth);
				});
			});
		},
		pageTitleSize: function(logoWidth){
			var windowWidth        = $(window).width(),
				headerOptionsWidth = $('.grve-header-options').length ? 360 : 0,
				$titleContainer    = $('.grve-page-title'),
				$title             = $('.grve-page-title .grve-title'),
				$subTitle          = $('.grve-page-title .grve-sub-title');

			$titleContainer.css( 'width' , windowWidth - logoWidth - headerOptionsWidth );
			if( $title.outerWidth() + $subTitle.outerWidth() < $titleContainer.outerWidth() ) {
				$titleContainer.addClass('grve-horizontal-title');
			} else {
				$titleContainer.removeClass('grve-horizontal-title');
			}
			headerSettings.heightFixes();
		},
		heightFixes: function(){
			// Resets
			$('.grve-page-title').css({ 'padding-top': 0, 'padding-bottom': 0 });
			$('#grve-header .grve-logo').css( 'line-height' , 'inherit' );
			$('.grve-header-options').css( 'line-height' , 'inherit' );

			var $logo           = $('#grve-header .grve-logo'),
				$titleContainer = $('.grve-page-title'),
				$headerOptions  = $('.grve-header-options'),
				titleHeight     = $titleContainer.height(),
				headerHeight    = $('#grve-header').height();

			if( titleHeight < headerHeight ) {
				var padding = ( headerHeight - titleHeight )/2;
				$titleContainer.css({ 'padding-top': padding, 'padding-bottom': padding });
				headerSettings.showTitle();
			} else {
				$logo.css( 'line-height' , ( titleHeight + 10 ) + 'px' );
				$headerOptions.css( 'line-height' , ( titleHeight + 10 ) + 'px' );
				$titleContainer.css({ 'padding-top': 5, 'padding-bottom': 5 });

				// Run Again Fullscreen
				if( $( '#grve-feature-section' ).length && $( '#grve-feature-section' ).hasClass('grve-fullscreen') )
				featureSection.fullScreen({initHeight : titleHeight + 10 });
				headerSettings.showTitle();
			}
			headerSettings.sickyHeader();
		},
		showTitle: function() {
			var $titleContainer = $('.grve-page-title'),
				$subTitle       = $('.grve-page-title .grve-sub-title');
			$titleContainer.animate({ 'opacity' : 1 },200,
				function(){
					$subTitle.find('span').addClass('active');
				});
		},
		sickyHeader: function() {
			var $headerWrapper = $('#grve-header-wrapper'),
				wpBarHeight    = $( 'body' ).hasClass( 'admin-bar' ) ? 32 : 0;

			$headerWrapper.css( 'height', $('#grve-header').height() );
			if(!verticalMenu){
				$('#grve-header').css( 'top', wpBarHeight );
				return;
			}
			if( $('#grve-feature-section').length ) {
				var headerOffset = $('#grve-feature-section').height();
				$(window).on('scroll', function() {
					var scroll = $(window).scrollTop();
					if( scroll > headerOffset ) {
						$('#grve-header').addClass('sticky').css( 'top', wpBarHeight );
					} else {
						$('#grve-header').removeClass('sticky').css( 'top', 0 );
					}
				});
			} else {
				$('#grve-header').addClass('sticky').css( 'top', wpBarHeight );
			}
		}
	};

	// -------------------------------------------------------------------------------------------
	// MAIN MENU
	// -------------------------------------------------------------------------------------------
	var mainMenu = {

		init: function(){

			mainMenu.config = {
				$nav : $('.grve-vertical-menu, .grve-horizontal-menu')
			};

			if( verticalMenu ) {
				mainMenu.vertical();
			} else {
				mainMenu.horizontal();
				if(isMobile.any()) {
					$(window).on("orientationchange",function(){
						mainMenu.horizontal();
					});
				} else {
					$(window).smartresize( function() {
						mainMenu.horizontal();
					});
				}
			}
		},
		vertical: function(){

			var $openButton  = $('#grve-header').find( '.grve-menu-btn' ),
				$closeButton = $('.grve-close-btn '),
				menuItem     = mainMenu.config.$nav.find(' > ul > li '),
				subMenu      = menuItem.find(' ul ');

			// Open Menu
			$openButton.click(function(e){

				e.preventDefault();
				$('#grve-wrapper').toggleClass('menu-open');
				mainMenu.config.$nav.css('display','block');

				if(Modernizr.csstransitions){
					mainMenu.config.$nav.find(' > ul ').transition({ x: 0 },500,'cubic-bezier(0,0.9,0.3,1)', {queue: false});
					$closeButton.transition({ x: 0, delay: 900 },300,'cubic-bezier(0,0.9,0.3,1)');
					menuItem.each(function(i) {
						$(this).transition({ x: 0, delay:i*200 },600,'cubic-bezier(0,0.9,0.3,1)', {queue: false});
					});
				}

			});
			// Close Menu
			$closeButton.click(function(e){

				e.preventDefault();
				$('#grve-wrapper').toggleClass('menu-open');

				if(!Modernizr.csstransitions) {
					subMenu.css('display','none');
					menuItem.removeClass('active');
					$('.grve-mainmenu-btn').removeClass('active');
					mainMenu.config.$nav.css('display','none');
				} else {
					menuItem.transition({ x: 320 },0);
					$(this).transition({ x: 320 },600,'cubic-bezier(0,0.9,0.3,1)', {queue: false});
					mainMenu.config.$nav.find(' > ul ').transition({ x: 320 },600,'cubic-bezier(0,0.9,0.3,1)',function(){

						subMenu.css('display','none');
						menuItem.removeClass('active');
						$('.grve-mainmenu-btn').removeClass('active');
						mainMenu.config.$nav.css('display','none');

					});
				}

			});

			mainMenu.addArrows();

			menuItem.find('a').unbind('click').bind('click', function(e) {
				if($(this).attr('href') == '#'){
					e.preventDefault();
					$(this).parent().find(' > .sub-menu, > .children ').slideToggle(200);
					$(this).parent().find('> .grve-mainmenu-btn').toggleClass('active');
				}
			});

		},
		addArrows: function(){

			var arrows = $('<div/>', {'class':'grve-mainmenu-btn'});
			arrows.prependTo('.menu-item-has-children, .page_item_has_children');
			$('.grve-mainmenu-btn').unbind('click').bind('click', function() {
				$(this).toggleClass('active').parent().find(' > .sub-menu, > .children ').slideToggle(200);
			});

		},
		horizontal: function(){

			var menuItem = mainMenu.config.$nav.find(' ul li');

			if( $(window).width() <= grveResponsiveThreshold ) {
				mainMenu.mobileMenu(menuItem);
				return;
			}
			menuItem.find( 'ul' ).css( 'display','block' );
			mainMenu.config.$nav.find('ul').css('display','block');
			$('#grve-header').removeClass('grve-mobile-menu');
			menuItem.unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
				mainMenu.adjustMenu($(this));
				$(this).toggleClass('hover').siblings().removeClass('hover');
			});

		},
		adjustMenu: function(menuItem){

			var menuItemPosition = menuItem.offset().left,
				menuItemWidth = menuItem.width(),
				subMenuWidth = menuItem.find(' ul ').width();
			menuItem.removeClass('grve-menu-item-right');
			if( menuItemPosition + menuItemWidth + subMenuWidth > $(window).width() ){
				menuItem.addClass('grve-menu-item-right');
			}
		},
		mobileMenu: function(menuItem){

			$('#grve-header').addClass('grve-mobile-menu');
			$('.grve-mainmenu-btn').removeClass('active');

			mainMenu.config.$nav.find('ul').css('display','none');
			mainMenu.config.$nav.find('ul li').unbind('mouseenter mouseleave');
			$('.grve-menu-btn').unbind('click').bind('click', function(e) {

				// DL: Suche mit ausblenden
				$('body').removeClass('searchvisible');

				e.preventDefault();
				$(this).toggleClass('active');
				$('.grve-menu').slideToggle();
			});
			menuItem.find('a').unbind('click').bind('click', function(e) {
				if($(this).attr('href') == '#'){
					e.preventDefault();
					$(this).parent().find(' > .sub-menu, > .children ').slideToggle(200);
					$(this).parent().find('> .grve-mainmenu-btn').toggleClass('active');
				}
			});
			if( !$('.grve-mainmenu-btn').length ){
				mainMenu.addArrows();
			}
		}
	};

	// -------------------------------------------------------------------------------------------
	// ISOTOPE
	// -------------------------------------------------------------------------------------------
	var isotopeSettings = {

		init: function(){

			var $element = $('.grve-isotope-container');

			if (!$element[0]) {
				return;
			}

			$element.each(function(){

				var $container  = $(this),
					layout      = $(this).attr('data-layout'),
					isotopeItem = $(this).find('.grve-isotope-item'),
					images      = $container.find('img');

				imagesLoaded(images,function(){
					$container.isotope({
						itemSelector: isotopeItem,
						layoutMode: layout,
						transformsEnabled: false
					});
					isotopeSettings.fixColumns( $container );
					$container.isotope('reLayout');
					$container.addClass('grve-isotope-active');

				});

				$container.parent().find('.grve-filter li').click(function(){
					var selector = $(this).attr('data-filter');
					$container.isotope({
						filter: selector
					});
					$(this).addClass('selected').siblings().removeClass('selected');
				});
				$(window).smartresize( function() {
					isotopeSettings.fixColumns( $container );
					$element.isotope('reLayout');
				});
			});
		},
		fixColumns: function( container ){

			var containerWidth = container.width(),
				$item          = container.find('.grve-isotope-item'),
				columns        = parseInt(container.attr('data-columns'));
			if( container.parent().is('.grve-portfolio.grve-fullwidth-element')) {
				columns = 5;
			}
			$item.css( 'width', parseInt(containerWidth / columns ),10);
		}

	};
	// -------------------------------------------------------------------------------------------
	// STAMP GALLERY
	// -------------------------------------------------------------------------------------------
	var stamp = {

		init: function(){

			var $element = $('.grve-stamp-masonry');

			if (!$element[0]) {
				return;
			}

			$element.each(function(){

				var $container   = $(this),
					$cornerStamp = $container.find('.grve-corner-stamp'),
					$stampItem   = $(this).find('.grve-stamp-element'),
					images       = $container.find('img');

				imagesLoaded(images,function(){

					var stampItemWidth = stamp.fixColumns( $container );
					stamp.setup( $container, $stampItem ,stampItemWidth ,$cornerStamp );

					$container.addClass('grve-isotope-active');
				});
				$(window).smartresize( function() {
					var stampItemWidth = stamp.fixColumns( $container );
					stamp.setup( $container, $stampItem ,stampItemWidth ,$cornerStamp );
				});
			});
		},
		setup: function( container, stampItem ,stampItemWidth, cornerStamp ){
			container.isotope({
				itemSelector: stampItem,
				masonry: {
					columnWidth: stampItemWidth,
					cornerStampSelector: cornerStamp
				},
				transformsEnabled: false
			});

			container.isotope('reLayout');
		},

		fixColumns: function( container ){

			var containerWidth = container.width(),
				$item          = container.find('.grve-stamp-element'),
				$cornerItem    = container.find('.grve-corner-stamp'),
				columns        = parseInt(container.attr('data-columns'));

			$item.css( 'width', parseInt(containerWidth / columns ),10);
			$cornerItem.css( 'width', ( $item.width() * 2 ) + 4 );
			return parseInt(containerWidth / columns );
		}

	};
	// -------------------------------------------------------------------------------------------
	// SLIDER
	// -------------------------------------------------------------------------------------------
	var  slider = {
		init: function(callback) {

			var elements  = [],
				$elements = $('.grve-main-content .grve-slider');

			if (!$elements[0]) {
				return callback() ;
			}

			$elements.each(function(){

				var sliderElement = $(this);
				elements.push(this);
				// Carousel Settings
				slider.sliderInit( sliderElement , function () {

					elements.splice(elements.indexOf(this), 1);

					if (!elements.length) {
						callback();
					}
				}.bind(this));

			});
		},
		sliderInit: function( slider, callback ){

			var $images     = slider.find('img'),
				sliderSpeed = parseInt( slider.attr('data-slider-speed') ) ? parseInt( slider.attr('data-slider-speed') ) : 3500,
				sliderNav   = slider.attr('data-slider-dirnav') == 'no' ? false : true;

			imagesLoaded($images,function(){
				slider.flexslider({
					animation : 'fade',
					controlNav : false,
					smoothHeight : false,
					animationSpeed : 500,
					easing : 'linear',
					nextText : ' ',
					prevText : ' ',
					directionNav : sliderNav,
					pauseOnHover : false,
					animationLoop : true,
					useCSS : true,
					slideshowSpeed : sliderSpeed,
					start : callback
				});
			});
		}
	};
	// -------------------------------------------------------------------------------------------
	// SECTIONS SETTINGS
	// -------------------------------------------------------------------------------------------
	var sectionSettings = {
		init: function(){
			var $element = $('.grve-main-content .grve-section, .grve-partner-advanced li');

			sectionSettings.settings( $element );
			$(window).smartresize( function() {
				sectionSettings.settings( $element );
			});

		},
		settings: function( element ){
			var windowWidth    = $(window).width(),
				contentWidth   = element.parent().width(),
				sidebarWidth   = $('#grve-sidebar').length ? $('#grve-sidebar').outerWidth() : 0,
				conteinerWidth = contentWidth + sidebarWidth,
				space          = (windowWidth - conteinerWidth)/2,
				sidebarSpace   = windowWidth - contentWidth;

			if( $('.grve-right-sidebar').length ) {
				element.css({'padding-left':space, 'padding-right': sidebarSpace, 'margin-left': -space, 'margin-right': -sidebarSpace});
			}
			else if( $('.grve-left-sidebar').length ) {
				element.css({'padding-left':sidebarSpace, 'padding-right': space, 'margin-left': -sidebarSpace, 'margin-right': -space});
			} else {
				element.css({'padding-left':space, 'padding-right': space, 'margin-left': -space, 'margin-right': -space});
			}
		}
	};
	// -------------------------------------------------------------------------------------------
	// FULL WIDTH ELEMENT
	// -------------------------------------------------------------------------------------------
	var fullWidthElement = {
		init: function(){
			var $element = $('.grve-main-content .grve-fullwidth-element');
			fullWidthElement.settings( $element );
			$(window).smartresize( function() {
				fullWidthElement.settings( $element );
			});
		},
		settings: function( element ){
			var windowWidth    = $(window).width(),
				contentWidth   = element.parent().width(),
				sidebarWidth   = $('#grve-sidebar').length ? $('#grve-sidebar').outerWidth() : 0,
				conteinerWidth = contentWidth + sidebarWidth,
				space          = (windowWidth - conteinerWidth)/2,
				sidebarSpace   = windowWidth - contentWidth;
			if( $('.grve-right-sidebar').length ) {
				element.css({'padding-left':0, 'padding-right': sidebarSpace, 'margin-left': -space, 'margin-right': -sidebarSpace});
			}
			else if( $('.grve-left-sidebar').length ) {
				element.css({'padding-left':sidebarSpace, 'padding-right': 0, 'margin-left': -sidebarSpace, 'margin-right': -space});
			} else {
				element.css({'padding-left':0, 'padding-right': 0, 'margin-left': -space, 'margin-right': -space});
			}
		}
	};

	// -------------------------------------------------------------------------------------------
	// MAIN FUNCTIONS
	// -------------------------------------------------------------------------------------------
	var mainFunctions = {

		init: function(){

			// TESTIMOIAL
			mainFunctions.testimonial();

			// LOAD BACKGROUND IMAGE
			mainFunctions.bgLoader();

			//  ANIMATED BACKGROUND
			mainFunctions.animatedBg();

			// MAGNIFIC POPUP
			mainFunctions.magnificPoup();

			// ANIMATIONS
			mainFunctions.animations();

			// CAROUSEL
			mainFunctions.carousel();
			$(window).resize(mainFunctions.carousel());

			// FIT VIDEO
			mainFunctions.fitVid();

			// FILTERS
			mainFunctions.filters();

			// SOCIALS SHARE
			mainFunctions.socialShareLinks();

			// HOVERS
			mainFunctions.hovers();

			// FIXED SIDEBAR
			mainFunctions.fixedSidebar();

			// PROGRESS BAR
			mainFunctions.progressBars();

			// ACCORDION TOGGLE
			mainFunctions.accordionToggle();

			// TABS
			mainFunctions.tabs();

			// INFO BOX
			mainFunctions.infoBox();

			// COUNTER
			mainFunctions.counter();

			// TEAM
			mainFunctions.team();

			// PRODUCTS
			mainFunctions.products();

			// PARTNERS
			mainFunctions.advancedPartners();

			// CONTACT FORM
			mainFunctions.contactForm();

			// LINKS
			mainFunctions.linksSettings();

			//BACK TO TOP
			mainFunctions.backtoTop();

			//VIDEO BG
			this.videoBg();
		},

		testimonial: function(){

			$('.grve-testimonial-carousel').each(function(){
				var sliderSpeed  = parseInt( $(this).attr('data-slider-speed') ) ? parseInt( $(this).attr('data-slider-speed') ) : 3500,
					smoothHeight = $(this).attr('data-slider-smoothheight') == 'no' ? false : true;

				$(this).flexslider({
					animation: 'slide',
					controlNav: true,
					animationLoop: true,
					slideshowSpeed: sliderSpeed,
					pauseOnHover: true,
					directionNav: false,
					smoothHeight: smoothHeight
				});
			});

		},

		bgLoader: function() {

			$('.grve-section .grve-bg-image','#grve-theme-body').each(function () {
				function imageUrl(input) {
					return input.replace(/"/g,"").replace(/url\(|\)$/ig, "");
				}
				var image = new Image(),
					$that = $(this);
				image.src = imageUrl($that.css('background-image'));
				image.onload = function () {
					$that.parent().css('z-index',0);
					$that.css('z-index',0).animate({'opacity':1},300);
				};
			});

		},

		animatedBg: function(){

			if($('.grve-animated-bg').length){
				var bgPosition = 0;
				setInterval(function(){
					bgPosition++;
					$('.grve-animated-bg').css('background-position',bgPosition+'px 0px');
				},75);
			}

		},

		parallaxBg: function(){

			if( ($('.grve-parallax-bg').length) && !isMobile.any() ){
				$(window).stellar({
					horizontalScrolling: false,
					verticalOffset: 0,
					horizontalOffset: 0,
					responsive: true,
					scrollProperty: 'scroll',
					positionProperty: 'transform',
					parallaxBackgrounds: false,
					parallaxElements: true,
					hideDistantElements: false,
				});
				mainFunctions.settings();
				$(window).smartresize( function() {
					mainFunctions.settings();
				});
			}

		},
		settings: function(){

			$('.grve-parallax-bg').each(function (){
				var $parallax       = $(this),
					$container      = $parallax.parent(),
					containerHeight = $container.outerHeight(),
					windowHeight    = $(window).height();

				$parallax.css({
					'height': containerHeight + windowHeight,
					'top': -1 * windowHeight * 0.5 / 2
				});

			});
		},
		magnificPoup: function(){

			//IMAGE
			$('.grve-image-popup').magnificPopup({
				type: 'image',
				fixedContentPos: false,
				fixedBgPos: false,
				mainClass: 'mfp-no-margins mfp-with-zoom',
				image: {
					verticalFit: true
				},
				zoom: {
					enabled: true,
					duration: 300
				}
			});

			//GALLERY
			$('.grve-gallery-popup').magnificPopup({
				delegate: 'a',
				type: 'image',
				fixedContentPos: false,
				fixedBgPos: false,
				tLoading: 'Loading image #%curr%...',
				mainClass: 'mfp-img-mobile',
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0,1] // Will preload 0 - before current, and 1 after the current image
				},
				image: {
					tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
					titleSrc: function(item) {
						var caption = item.el.find('.grve-hover-title');
						var title = caption.attr('title') ? caption.html() + '<small>' + caption.attr('title') + '</small>' : caption.html();
						return title;
					}
				},
				zoom: {
					enabled: true,
					duration: 300
				}
			});

			//VIDEOS
			$('.grve-youtube-popup, .grve-vimeo-popup, .grve-video-popup, .grve-page-popup').magnificPopup({
				disableOn: 0,
				type: 'iframe',
				fixedContentPos: false,
				fixedBgPos: false,
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false
			});

			//INLINE TYPE
			$('.grve-header-options-popup').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: false,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'mfp-bg my-mfp-zoom-in'
			});
		},

		animations: function(){

			var animItem = $('.grve-animated-item');
			if(isMobile.any()) {
				animItem.css('opacity',1);
			} else {
				animItem.each(function() {
					var timeDelay = $(this).attr('data-delay');
					$(this).appear(function() {
						var $that = $(this);
						setTimeout(function () {
							$that.addClass('animated');
						}, timeDelay);
					},{accX: 0, accY: -150});
				});
			}
		},

		carousel: function() {

			$('.grve-carousel').each(function(){
				var carousel     = $(this),
					carouselItem = carousel.find('.grve-carousel-item'),
					element      = carousel.find('img'),
					columns      = parseInt($(this).attr('data-columns'));

				this.calculateColumns = function(){

					if($(window).width() < 960 && $(window).width() > 460) {
						columns = 2;
					} else if($(window).width() < 460) {
						columns = 1;
					} else {
						columns = parseInt($(this).attr('data-columns'));
					}
				};
				this.calculateColumns();

				imagesLoaded(element,function(){

					var itemWidth = carousel.width() / columns;
					carousel.animate({'opacity':1});

					carousel.carouFredSel({
						auto: true,
						scroll : {
							fx: 'scroll',
							duration: 800,
							easing: 'easeInOutCubic',
							pauseOnHover: true
						},
						prev: '#prev2',
						next: '#next2',
						pagination: '#pager2',
						circular: true,
						responsive: true,
						padding     : 10,
						align       : 'center',
						swipe: {
							onMouse: true,
							onTouch: true
						},
						items: {
							height : 'auto',
							width  : itemWidth,
							visible: columns
						}
					},{
						wrapper: {
							element: 'div',
							classname: 'grve-carousel-inner'
						}
					});

				});

				if(isMobile.any()){
					carouselItem.each(function(){

						var carouselLink      = $(this).find('.grve-btn');
						carouselLink.clone().attr('class','grve-carousel-btn').empty().prependTo($(this));

					});
				}

			});
		},
		fitVid: function(){

			$('.grve-video, .grve-media').fitVids();
			$('iframe[src*="youtube"]').parent().fitVids();
			$('iframe[src*="vimeo"]').parent().fitVids();

		},

		filters: function(){

			var $filter = $( '.grve-blog-label .grve-filter li' );

			if( $filter.parents( '.grve-related-post' ).length ) {
				return;
			}

			$filter.each( function() {
				var filterWidth = $(this).outerWidth();

				if( $(this).hasClass( 'selected' )) {
					$(this).css({ 'width' : filterWidth + 10 });
				} else {
					$(this).css({ 'width' : 30 });
				}

				$(this).hover(function(){
					$(this).css({ 'width' : filterWidth  + 10 });
				},function(){
					if( $(this).hasClass( 'selected' ) ) return;
					$(this).css({ 'width' : 30 });

				});

				$(this).click( function() {
					$(this).css({ 'width' : filterWidth  + 10 })
						.siblings().css({ 'width' : 30 });
				});

			});

		},
		socialShareLinks: function(){

			$('.grve-social-share-facebook').click(function (e) {
				e.preventDefault();
				window.open( 'https://www.facebook.com/sharer/sharer.php?u=' + $(this).attr('href'),
					"facebookWindow",
					"height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" );
				return false;
			});

			$('.grve-social-share-twitter').click(function (e) {
				e.preventDefault();
				window.open( 'http://twitter.com/intent/tweet?text=' + encodeURIComponent( $(this).attr('title') ) + ' ' + $(this).attr('href'),
					"twitterWindow",
					"height=450,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" );
				return false;
			});

			$('.grve-social-share-linkedin').click(function (e) {
				e.preventDefault();
				window.open( 'http://www.linkedin.com/shareArticle?mini=true&url=' + $(this).attr('href') + '&title=' + encodeURIComponent( $(this).attr('title') ),
					"linkedinWindow",
					"height=500,width=820,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" );
				return false;
			});

			$('.grve-social-share-googleplus').click(function (e) {
				e.preventDefault();
				window.open( 'https://plus.google.com/share?url=' + $(this).attr('href'), "googleplusWindow",
					"height=600,width=600,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" );
				return false;
			});

			$('.grve-social-share-pinterest').click(function (e) {
				e.preventDefault();
				window.open( 'http://pinterest.com/pin/create/button/?url=' + $(this).attr('href') + '&media=' + $(this).data('pin-img') + '&description=' + encodeURIComponent( $(this).attr('title') ),
					"pinterestWindow",
					"height=600,width=600,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" );
				return false;
			});

		},

		hovers: function(){

			$('.grve-hover-item.grve-default-hover').each( function() {
				$(this).hoverdir();
			});
			$('.grve-portfolio.grve-default-hover .grve-hover-item').each( function() {
				$(this).hoverdir();
			});

		},

		fixedSidebar: function(){

			var sidebar      = $('.grve-sidebar.grve-fixed-sidebar'),
				content      = $('.grve-main-content'),
				contentWidth = content.outerWidth(),
				top          = 150,
				margin       = 0;


			if(!$('.grve-left-sidebar').length) {
				margin = contentWidth +30;
			}

			$(window).on('scroll', function() {

				if(!sidebar.length || sidebar.height() >= content.height() ) return;
				var contentTop    = content.offset().top,
					contentHeight = content.height(),
					sidebarHeight = sidebar.height(),
					contentBottom = contentTop + contentHeight;

					if( $(window).scrollTop() > contentTop - top && $(window).scrollTop() < contentBottom - ( top + sidebarHeight )){
						sidebar.css({'position':'fixed', 'margin-left':margin, 'top':top, 'bottom':'auto'});
					}
					else if( $(window).scrollTop() > contentTop ){
						sidebar.css({'position':'absolute', 'margin-left':margin, 'top':'auto', 'bottom':0});
					}
					else if( $(window).scrollTop() < contentTop ){
						sidebar.css({'position':'static', 'margin-left':0, 'top':'auto', 'bottom':0});
					}
			});

		},

		progressBars: function(){

			$('.grve-progress-bar').each(function() {
				var val        = $(this).attr('data-value'),
					percentage = $('<div class="grve-percentage">'+ val + '%'+'</div>');
				$(this).find('.grve-bar-line').delay(1000).animate({width: val + '%'}, 1200, 'easeOutBack');
				percentage.appendTo($(this).find('.grve-bar'));
				$(this).find('.grve-percentage').delay(1200).animate({left: val + '%'}, 1200, 'easeOutBack');
			});

		},

		accordionToggle: function(){

			$('.grve-toggle-wrapper.grve-first-open').each(function(){
				$(this).find('li').first().addClass('active');
			});

			$('.grve-toggle-wrapper li.active').find('.grve-title').addClass('active');
			$('.grve-toggle-wrapper li .grve-title').click(function () {
				$(this)
					.toggleClass('active')
					.next().slideToggle(350);
			});


			$('.grve-accordion-wrapper.grve-first-open').each(function(){
				$(this).find('li').first().addClass('active');
			});

			$('.grve-accordion-wrapper li.active').find('.grve-title').addClass('active');
			$('.grve-accordion-wrapper li .grve-title').click(function () {
				$(this)
					.toggleClass('active').next().slideToggle(350)
					.parent().siblings().find('.grve-title').removeClass('active')
					.next().slideUp(350);
			});


		},

		tabs: function(){

			this.titleSize = function(){
				$('.grve-horizontal-tab').each(function(){
					var numberTitles = $(this).find('.grve-tabs-title li').size(),
						tabTitles    = $(this).find('.grve-tabs-title');

					if($(window).width() < 641 ){
						$(this).find('.grve-tabs-title li').css('width', '');
					} else {
						$(this).find('.grve-tabs-title li').css('width', tabTitles.width() / numberTitles).parent().animate({'opacity':1},900);
					}
				});

				$('.vc_tta-tabs.vc_tta-tabs-position-top').each(function(){
					var numberTitles = $(this).find('.vc_tta-tabs-list li').size();

					if($(window).width() < 641 ){
						$(this).find('.vc_tta-tabs-list li').css('width', '');
					} else {
						$(this).find('.vc_tta-tabs-list li').css('width', 100 / numberTitles + "%").parent().animate({'opacity':1},900);
					}
				});
			};
			this.titleSize();
			$(window).resize(this.titleSize);

			$('.grve-tabs-title li').click(function () {
				$(this).addClass('active').siblings().removeClass('active');
				$(this).parent().parent().find('.grve-tabs-wrapper').find('.grve-tab-content').eq($(this).index()).addClass('active').siblings().removeClass('active');
			});
			$('.grve-tabs-title').each(function(){
				$(this).find('li').first().click();
			});


		},

		infoBox: function(){
			var infoMessage = $('.grve-message'),
				closeBtn    = infoMessage.find($('.grve-close'));
			closeBtn.click(function () {
				$(this).parent().slideUp(150);
			});
		},

		counter: function(){
			var selector = '.grve-counter-item span';
			$(selector).each(function(i){
				var elements = $(selector)[i];
				$(elements).attr('id','grve-counter-' + i );
				var delay = $(this).parents('.grve-counter').attr('data-delay') !== '' ? parseInt( $(this).parents('.grve-counter').attr('data-delay') ) : 200,
					options = {
						useEasing    : true,
						useGrouping  : true,
						separator    : ',',
						decimal      : '.',
						prefix       : $(this).attr('data-prefix') != '' ? $(this).attr('data-prefix') : '',
						suffix       : $(this).attr('data-suffix') != '' ? $(this).attr('data-suffix') : ''
					},
					counter = new countUp( $(this).attr('id') , $(this).attr('data-start-val'), $(this).attr('data-end-val'), $(this).attr('data-decimal-points'), 2.5, options);
				$(this).appear(function() {
					setTimeout(function () {
						counter.start();
					}, delay);
				});
			});
		},

		team: function(){

			$( '.grve-team-item ').each(function () {
				var $teamMedia  = $(this).find('.grve-team-media'),
					$teamPerson = $(this).find('.grve-team-person'),
					$teamSocial = $(this).find('.grve-team-social');

				function teamSize() {
					$teamMedia.css( 'height' , $teamMedia.width() );
				}
				teamSize();

				$(window).smartresize( teamSize );

				$(this).hover( function(){
					$teamPerson.stop().animate({ 'top' : - $teamSocial.height() },{
						queue :    false,
						duration : 200,
						easing :  'easeOutQuart'
					});
				}, function() {
					$teamPerson.stop().animate({ 'top' : 0 },{
						queue:    false,
						duration: 400,
						easing:  'easeInOutQuad'
					});
				});
			});

		},

		products: function(){

			$('.grve-product-item').each(function () {
				var $that          = $(this),
					productMedia   = $that.find('.grve-product-media'),
					productImage   = $that.find('.grve-product-media > a'),
					productOptions = $that.find('.grve-product-options');

				function grve_product_size() {
					productMedia.css('height',productMedia.width());
				}
				if(isMobile.any()) {
					productImage.click(function(e){
						e.preventDefault();
					});
				}
				grve_product_size();
				$(window).resize(grve_product_size);

				$that.hover(function(){
					productImage.stop().animate({'top': - productOptions.height()},{queue:false,duration:200, easing:'easeOutQuart'});
				}, function() {
					productImage.stop().animate({'top': 0},{queue:false,duration:400, easing:'easeInOutQuad'});
				});
			});

			$('.grve-product-options .add_to_cart_button').click(function(){
				$(this).parents('.grve-product-media').addClass('grve-product-added-to-cart');
			});

		},

		advancedPartners: function(){

			$('.grve-section').each(function () {
				var partners       = $(this).find('.grve-partner-advanced'),
					partnersNumber = partners.size(),
					cnt = -1;

				partners.each(function () {

					cnt++;
					$(this).find('.grve-partner-color').css('opacity',cnt * (1 / partnersNumber));
				});

			});

			$('.grve-partner-advanced').each(function() {

				$(this).click(function(){

					$(this).find('.grve-partner-content').slideToggle(600, 'easeOutBack');
				});

			});

		},
		contactForm: function(){

			$('#grve-comment-submit-button').addClass('grve-btn grve-primary grve-btn-small');

			//Reset Form
			$.fn.resetForm = function () {
			  $(this).each (function() { this.reset(); });
			};

			//Contact Form
			$('.grve-form-button').click(function(e){

				//stop the form from being submitted
				e.preventDefault();
				var sendButton  = $(this);
				var currentForm = $(this).parent();

				var nameElem  = currentForm.find('.grve-form-name').first();
				var emailElem = currentForm.find('.grve-form-email').first();
				var emailReg  = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

				currentForm.find('.grve-form-result').removeClass('active').html('');
				if ( '' === nameElem.val() ){
					currentForm.find('.grve-form-result').addClass('active').html(grve_form_data.name_required_error);
					nameElem.focus();
				} else if ( !emailReg.test( emailElem.val() ) || '' === emailElem.val() ) {
					currentForm.find('.grve-form-result').addClass('active').html(grve_form_data.email_validation_error);
					emailElem.focus();
				} else {
					sendButton.html(grve_form_data.sending_label);
					var newCustomerForm = currentForm.serialize();
					var ajaxurl = grve_form_data.ajaxurl;

					$.ajax({type: 'POST', url: ajaxurl, data: 'action=grve_form_send&' + newCustomerForm, success: function(result) {
						//and after the ajax request ends we check the text returned
						if ( 'sent' == result ) {
							currentForm.find('.grve-form-result').addClass('active').html( grve_form_data.success );
							sendButton.html( grve_form_data.send_label );
							sendButton.parent().resetForm();
						} else {
							currentForm.find('.grve-form-result').addClass('active').html( grve_form_data.failed );
							sendButton.html( grve_form_data.send_label );
							sendButton.parent().resetForm();
						}
					}});
				}
			});

		},
		linksSettings: function(){

			var topOffset = !isMobile.any() ? $('#grve-header').height() : 0;

			/* JT Anchor Link problem
			$('a[href="#"]').click(function(e) {
				e.preventDefault();
			});


			$('a[href*="#"]:not( [href="#"], .grve-header-options-popup )').click(function(e) {

				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname ) {
					var target = $(this.hash);
					target     = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if ( target.length && target.hasClass('grve-section') ) {
						$('html,body').animate({
							scrollTop: target.offset().top - topOffset
						}, 1000);
						window.location.hash = "";
						return false;
					} else {
						e.preventDefault();
					}
				}
			});
			*/

			if (window.location.hash) {
				setTimeout(function() {
					$('html, body').scrollTop(0);
					$('.grve-main-content').css('visibility','visible');
					var target = window.location.hash;

					if ($(target).length) {
						$('html, body').delay(600).animate({
							scrollTop: $(target).offset().top - topOffset
						}, 600);
					}
					window.location.hash = "";
				}, 0);
			}
			else {
				$('.grve-main-content').css('visibility','visible');
			}
		},
		backtoTop: function(){

			var btnUp = $('<div/>', {'class':'grve-top-btn'});
				btnUp.appendTo('#grve-wrapper');

			$('.grve-top-btn').click(function(){
				$('html, body').animate({scrollTop: 0}, 1200, 'easeInOutQuad');
			});

			$(window).on('scroll', function() {
				if ($(this).scrollTop() > 600)
					$('.grve-top-btn').addClass('active');
				else
					$('.grve-top-btn').removeClass('active');
			});
		},
		videoBg: function(){
			if(!isMobile.any()){
				$('.grve-video-bg-element').css('display','block');
				$('.grve-video-bg-element').each(function(){
					var video = $(this),
						videoHeight = video.height(),
						videoSectionHeight = video.parent().outerHeight();

					//set video top position
					if( videoSectionHeight >= videoHeight ){
						video.css('top', -(videoSectionHeight - videoHeight) /2 );
					} else {
						video.css('top', -videoSectionHeight /2 );
					}
				});
			} else{
				$('.grve-video-bg-element').css('display','none');
			}

		},
	};

	// -------------------------------------------------------------------------------------------
	// GLOBAL VARIABLES
	// -------------------------------------------------------------------------------------------
	var verticalMenu = $('#grve-header').hasClass('grve-style-1') ? true : false;
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};


	$(document).ready( function() {

		//Parallax Slider
		$('.scene').parallax();

		// MAIN MENU
		mainMenu.init();


		// FEATURE SECTION
		if( $('#grve-feature-section').length ) {
			featureSection.init();
		}

		// SECTION SETTINGS
		sectionSettings.init();

		// FULL WIDTH ELEMENT
		fullWidthElement.init();

		// HEADER SETTINGS
		headerSettings.init();

		// SLIDER
		slider.init(function () {
			// ISOTOPE
			isotopeSettings.init();
		}.bind(this));

		// STAMP GALLERY
		stamp.init();

		// MAIN FUNCTIONS
		mainFunctions.init();

	});

	$(window).load( function() {

		// PARALLAX BG
		mainFunctions.parallaxBg();

	});


})(jQuery);