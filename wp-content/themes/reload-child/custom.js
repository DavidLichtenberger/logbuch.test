jQuery(document).ready( function($) {



	var _width = $(window).width();
	var _height = $(window).height();

	$(window).resize(function(e){
		_width = $(window).width();
		_height = $(window).height();
	});


	$('.sk-about-author-top img, .sk-about-author-top h5').click(function(e){
		e.preventDefault();
		var browserTop = $('body').find('.grve-about-author').offset().top;
		if (browserTop) {
			$('html, body').animate({
				scrollTop: (browserTop -  200)
			}, 500);
		}
	});


	// search form
	/* [jt 20.11.2017 ] off
	$('.header-social-media .sk-header-search-btn').parent().append( $('#grve-search-modal') );

	$(document).on('click', '.sk-header-search-btn', function(e){
		e.preventDefault();
		e.stopPropagation();

		$('#grve-search-modal').toggleClass('active');
	});

	$('.grve-search-btn i').removeClass('fa-search');
	$('.grve-search-btn i').addClass('fa-chevron-circle-right');

	$('.grve-isotope-item').each(function(){
		if ($(this).find('img').length || $(this).find('iframe').length) {
			$(this).addClass('has-image');
		}
	});
	*/


	// POSTS: move the author info above the post title
	$('.sk-about-author-top').prependTo('#grve-main-title-section');

	// select the post related category first
	var $el = $('.grve-related-post').find('.grve-filter li.selected');
	var selector = $el.attr('data-filter');
	var $container = $('.grve-related-post').find('.grve-isotope-container');
	var isotopeItem = $container.find('.grve-isotope-item');
	var columns = parseInt($container.attr('data-columns'));
	var layout = $el.attr('data-layout');
	var element = $container.find('img');

	/* [jt 20.11.2017 ] off
	var isotopeColumns = function(){
		if($(window).width() < 768 ) {
			columns = 1;
		} else if( $container.parent().hasClass('grve-blog grve-blog-label') && !$container.parents().parent().hasClass('grve-column-1')  && $container.parent().parent().parent().hasClass('grve-row') ){
			columns = 1;
		} else if( $container.parent().hasClass('grve-portfolio grve-fullwidth-element') ){
			columns = 5;
		}
		else {
			columns = parseInt($(this).attr('data-columns'));
		}
	}
	*/

	// isotopeColumns();
	/* [jt 20.11.2017 ] off
	imagesLoaded(element,function(instance){
		$container.isotope({
			itemSelector: isotopeItem,
			layoutMode: layout,
			transformsEnabled: false,
			filter: selector
		});
		$container.animate({'opacity': 1},1300);
	});
	*/

	$('.grve-top-btn').append('<i class="fa fa-chevron-up"></i>');

	var cat = $('#post-category').val();

	$('.grve-menu li').each(function(){
		var $el = $(this);
		if ($el.find('a').text() == cat) {
			$el.addClass('current-menu-item');
			$el.addClass('current-page-item');
		}
	});

	//$('#grve-theme-body #grve-main-title-section').next().css('min-height', ($(window).height() - 280) );


	//[jt 23.03.2016 ] sort related posts by tag per default
	$( window ).load(function() {
	  $('.grve-related-post li:nth-child(2)').removeClass('selected');
	  setTimeout(function(){
	  	$('.grve-related-post li:nth-child(3)').trigger('click');
	  }, 1500);
	});

	//[jt 15.02.2017 ] Bildrechte on single view
	$('.btn-visibility-copyrights').click(function(event) {
		event.preventDefault();
		$('.list-image-with-text').toggleClass('in');
	});


	//[jt 15.11.2017 ]reqires:  * waypoints + waypoints sticky
	if ( $( ".sk-indexnav" ).length ) {
		var sticky = new Waypoint.Sticky({
		  element: $('.sk-indexnav')[0]
		})
	}

	//[jt 15.11.2017 ]reqires:  * waypoints + waypoints sticky
	if ( $( ".sk-author-meta" ).length ) {
		var sticky = new Waypoint.Sticky({
		  element: $('.sk-author-meta')[0]
		})
	}

	//[dl 15.11.2017 ] single post sharing sticky
	if (($(window).width() > 959)
		&& $( ".no-touch .sk-post-meta-bar-upper" ).length
		&& $('.no-touch .grve-single-post.post').length) {

		var shareEl = $( ".no-touch .sk-post-meta-bar-upper" ).first();
		var contentEl = $('.no-touch .grve-single-post.post').first();

		var bufferHeight = 100;

		$(window).scroll( function() {

			var contentTop = contentEl.offset().top;
			var contentBottom = contentTop + contentEl.outerHeight();
			var shareHeight = shareEl.outerHeight();
			var shareTop = shareEl.offset().top;
			var shareBottom = shareTop + shareHeight;

			if (shareTop < contentTop - bufferHeight || shareBottom > contentBottom + bufferHeight) {
				if (shareEl.hasClass('show')) {
					shareEl.removeClass('show');
				}
			} else {
				if (!shareEl.hasClass('show')) {
				 	shareEl.addClass('show');
				}
			}

		});
	}

	//[jt 25.10.2017 ] home: Infinite scroll. reqires:  * waypoints + waypoints infinite

  if ( $( ".page-numbers.next" ).length ) {

  	$(' .grve-pagination').addClass('infinite');

  	var infinite = new Waypoint.Infinite({
  	      element: $('.sk-teaser-list')[0],
  	      items: '.sk-teaser',
  	      more: '.page-numbers.next',
  	      offset: 'bottom-in-view'
  	})

  }

	//[jt 15.11.2017] Menu Btn Show Searchbar Event
	$('.menu-item-16912').click(function(event) {
		event.preventDefault();
		$('body').toggleClass('searchvisible');
		$('#searchform #s').focus();
	});

	//[dl 3.12.2017] single: place author

	// get all `<p>`s, need the length for scoring below
	var pSort = $('.single-post .grve-post-content p');
	var pLength = pSort.length;

	pSort = pSort
		// score them
		.map( function(index, item) {

			// prefer `<p>`s in the upper area
			var score = 0 - Math.abs(pLength / 10 - index);
			// reduce score for `<p>`s with embeds and `<img>`s
			if ($(item).children('img, iframe').length) score -= 10000;
			// reduce score for short `<p>`s
			if ($(item).html().length < 300) score -= 1000;
			// reduce score for `<p>`s with classes (`intro`, etc.)
			if (item.hasAttribute('class')) score -= 100;

			//return { item, score };
			return {
				item: item,
				score: score
			};
		})

		// sort by score
		//.sort( (itemA, itemB) => itemB.score - itemA.score );
		.sort( function(itemA, itemB) { return (itemB.score - itemA.score) });

	// add author to best p
	if (pSort.length > 0) {
		$(pSort[0].item).wrap('<div class="wrap-author"></div>');
		$('.single-post .grve-about-author').prependTo('.wrap-author');

	// if something went wrong, remove author
	} else {
		$('.single-post .grve-about-author').remove();
	}

	// progress bar
	var $content = $('.grve-main-content .grve-single-post');
	if ($('.sk-progressbar').length > 0 && $content.length > 0) {

		var pbTimeout = false;

		$(window).scroll( function() {
			if (!pbTimeout) {
				console.log('scroll');
				var scroll = $(window).scrollTop();
				var scrollMiddle = scroll + $(window).innerHeight() / 2;
				var headerEnd = $('#grve-header-wrapper').outerHeight();
				if ($('#wpadminbar').length > 0) headerEnd += $('#wpadminbar').outerHeight();
				var contentStart = $content.offset().top;
				var contentHeight = $content.outerHeight();
				var progress = Math.min(1, Math.max(0, (scrollMiddle - contentStart) / contentHeight ) );
				if (scroll <= headerEnd) progress = 0;
				$('.sk-progressbar').css({transform: 'scaleX(' + progress + ')'});
				pbTimeout = window.setTimeout( function() {
					window.clearTimeout(pbTimeout);
					pbTimeout = false;
				}, 100);
			}
		});
	}

});
