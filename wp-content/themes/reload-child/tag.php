<?php
/**
 * The template for displaying Tags
 */

get_header();
?>

<section class="grve-section grve-blog-masonry">
	<div class="grve-container <?php echo grve_sidebar_class(); ?>">
		<!-- Content -->
		<div class="grve-main-content">

			<h1>Schlagwort: <span class="tags"><?php single_term_title( '' ); ?></span></h1>

			<?php get_template_part( 'template-parts/content', 'teaserlist' ); ?>

			<?php /* JT 27.11.17 replaced
			<div class="grve-isotope-container isotope" data-columns="3" data-layout="masonry">
				<?php while ( have_posts() ) : the_post();?>
					<article class="grve-isotope-item isotope-item">
						<div class="grve-post-content">
							<a href="<?php the_permalink()?>">
								<h5 class="grve-post-title"><?php the_title()?></h5>
							</a>
							<?php if( function_exists( 'get_coauthors' ) ): ?>
							 	<?php $coauthors = get_coauthors($post->ID);?>
							<?php endif;?>
							<div class="grve-post-meta">
								<div class="grve-post-author">
									<?php echo __( 'von', GRVE_THEME_TRANSLATE ) . '  '; ?>
									<?php $i = 0; ?>
									<?php foreach( $coauthors as $coauthor ): ?>
										<?php if ($i > 0): ?>
											<?php echo ' & '?>
										<?php endif;?>
										<?php $i++; ?>
										<?php echo $coauthor->display_name . ' '?>
									<?php endforeach; ?>
								</div>
							</div>
							<p><?php the_excerpt();?></p>
						</div>
					</article>
				<?php endwhile; ?>
			</div>
			*/?>

		</div>
		<!-- End Content -->

		<?php get_sidebar(); ?>

	</div>
</section>
<?php get_footer(); ?>
