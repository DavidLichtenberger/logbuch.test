<?php get_header(); ?>

<section class="grve-section grve-blog-masonry">
	<div class="grve-container">
		<!-- Content -->
		<div class="grve-main-content grve-align-center">

			<h1><?php _e("Ooops! Page Not Found", GRVE_THEME_TRANSLATE); ?></h1>
			<p>
				<?php _e( "Sorry! The page you are looking for wasn't found!", GRVE_THEME_TRANSLATE ); ?>
				<br>
				<?php _e( "Check again your spelling or write the content you are seeking for in the search field.", GRVE_THEME_TRANSLATE ); ?>
			</p>
			<div id="searchbar">
				<?php get_search_form(); ?>
			</div>

			<h2 class="teaser-list-title">Neuste Beitr&auml;ge</h2>
			<?php
			$args = array ( 'posts_per_page' => 3 );
			$pagination = false;
			query_posts( $args );
			require( locate_template( 'template-parts/content-teaserlist.php' ) );
			wp_reset_query();
			?>

		</div>
		<!-- End Content -->

	</div>
</section>

<?php get_footer(); ?>