<!doctype html>
<html class="no-js grve-responsive" <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title><?php bloginfo('name'); ?> <?php wp_title('|'); ?></title>

		<!-- viewport -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

		<!-- allow pinned sites -->
		<meta name="application-name" content="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />

		<?php /* JT[10.04.2016] replaced:
		$grve_favicon = grve_option('favicon','','url');
		if ( ! empty( $grve_favicon ) ) {
		*/ ?>
		<?php
			if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
				$grve_favicon = grve_option('favicon','','url');
				if ( ! empty( $grve_favicon ) ) {
					$grve_favicon = str_replace( array( 'http:', 'https:' ), '', $grve_favicon );
		?>
		<link href="<?php echo esc_url( $grve_favicon ); ?>" rel="icon" type="image/x-icon">
		<?php
				}
			}
		?>

		<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
		<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<?php wp_head(); ?>

 		<?php
 		$paged = (get_query_var('page')) ? get_query_var('page') : 1;

 	/* JT not needed?
 		if ( get_query_var('paged') ) {
			$paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
	    $paged = get_query_var('page');
		} else {
		  $paged = 1;
		}
	*/
	/* JT not needed?
		if ((int)$paged == 1 && is_front_page() || is_home()) {
			?>
			<script type="text/javascript">
				jQuery(document).ready(function($){
					$('.grve-pagination li').each(function(){
						var $el = $(this);
						var $link = $el.find('a');

						if ($link.length) {
							var href = $link.attr('href');

							if (href.indexOf('/home') < 0) {
								href = href.replace('seite', 'home/seite');
								$link.attr('href', href);
							}
						}

					});
				});
			</script>
			<?php
		}
	*/

		?>
		<!-- Google Tag Manager --
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-K9VC86J');</script>
		<!-- End Google Tag Manager -->
	</head>

	<body id="grve-body" <?php body_class(); ?>>

		<!-- Google Tag Manager (noscript) --
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9VC86J"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div id="grve-wrapper">

			<?php
			/*JT
				global $paged, $wp_query, $wp;

				if (is_front_page()) {
					if ( get_query_var('paged') ) {
						$paged = get_query_var('paged');
					} elseif ( get_query_var('page') ) {
				    $paged = get_query_var('page');
					} else {
					  $paged = 1;
					}

					$wp_query->set('paged', $paged);
				}
				*/
			?>

			<?php
				$grve_icon_color = grve_option( 'header_icon_color' );
				$grve_icon_color_class = 'grve-header-light';
				if ( 'dark' == $grve_icon_color ) {
					$grve_icon_color_class = 'grve-header-dark';
				}

				$grve_header_style_class = "grve-style-1" . " " . $grve_icon_color_class;
				if ( grve_option( 'menu_style', 'vertical' ) == 'horizontal' ) {
					$grve_header_style_class = "grve-style-2" . " " . $grve_icon_color_class;
				} else {
					grve_print_header_feature();
				}

			?>

			<div id="grve-theme-body">

				<?php grve_print_header_item_navigation(); ?>

				<?php /* complete replaced from:
				<header id="grve-header" class="<?php echo
				...
				</header>
				*/ ?>
				<header id="grve-header" class="<?php echo $grve_header_style_class; ?>" data-height="<?php echo grve_option('header_height','90'); ?>">
					<div id="sk-top-menu-container">
						<div class="grve-container">
						<?php
						/* JT erase dynamic Logo, instead hard-coded
							do_action( 'grve_header_before_logo' );
							$grve_logo = grve_option( 'logo','','url' );
							$grve_logo_width = grve_option( 'logo','','width' );
							$grve_logo_height = grve_option( 'logo','','height' );
							if( !empty( $grve_logo_width ) && !empty( $grve_logo_height ) ) {
								$grve_logo_dimensions = ' width="' . esc_attr( $grve_logo_width ) . '" height="' . esc_attr( $grve_logo_height ) . '"';
							} else {
								$grve_logo_dimensions = '';
							}

							$logo_tag_start = '<h1 class="grve-logo">';
							$logo_tag_end = '</h1>';
							if ( 0 == grve_option( 'logo_header_tag', 1 ) ) {
								$logo_tag_start = '<div class="grve-logo">';
								$logo_tag_end = '</div>';
							}

							if ( !empty( $grve_logo ) ) {
								$grve_logo = str_replace( array( 'http:', 'https:' ), '', $grve_logo );
								echo $logo_tag_start;
						*/
						?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<div class="sk-logo"></div>
							</a>

						<?php /*


								<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<?php
									$grve_retina_logo_data = '';
									$grve_retina_logo = grve_option( 'retina_logo','','url' );
									if ( !empty( $grve_retina_logo ) ) {
										$grve_retina_logo = str_replace( array( 'http:', 'https:' ), '', $grve_retina_logo );
										$grve_retina_logo_data .= ' data-at2x="' . esc_url( $grve_retina_logo ) . '"';
									}
								?>
									<img id="logo-large" src="<?php echo esc_url( $grve_logo ); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"<?php echo $grve_logo_dimensions . $grve_retina_logo_data; ?>>

								<?php ////JT[10.04.2016] custom: display small logo ?>
										<img id="image-logo-small"
											src="<?php echo dirname(get_bloginfo('stylesheet_url'))?>/logo-small.png"
											alt="<?php bloginfo('name'); ?>"
											title="<?php bloginfo('name'); ?>"
											data-at2x="<?php echo dirname(get_bloginfo('stylesheet_url'))?>/logo-small@2x.png"/>

								</a>
								<span><?php bloginfo('name'); ?></span>

						*/ ?>
						<?php /*
								echo $logo_tag_end;
							}
							*/
						//JT[10.04.2016] custom code from old version
						?>

						<ul class="header-social-media">
							<li>
								<a class="newsletter" href="/newsletter"></a>
							</li>
							<li>
								<a class="fa fa-facebook" href="https://www.facebook.com/LogbuchSuhrkamp" target="_blank"></a>
							</li>
							<li>
								<a class="fa fa-twitter" href="https://twitter.com/LogbuchSuhrkamp" target="_blank"></a>
							</li>

						</ul>
						<div class="sk-mobile-menu">
							<a href="#" class="fa fa-bars grve-menu-btn sk-menu-btn"></a>
						</div>
						<?php //endJT
							do_action( 'grve_header_after_logo' );
						?>

							<?php if ( 'vertical' == grve_option( 'menu_style', 'vertical' ) ) { ?>
							<?php grve_print_vertical_header_title(); ?>
							<?php } ?>

							<?php // grve_print_header_options(); ?>

							<?php
							if ( '1' == grve_option( 'menu_visibility', '1' ) ) {
								if ( 'horizontal' == grve_option( 'menu_style', 'vertical' ) ) {
							?>
								<nav class="grve-horizontal-menu">
									<?php grve_header_nav(); ?>
								</nav>
							<?php
								}
							} else {
								do_action( 'grve_header_horizontal_navigation_menu' );
							}
							?>
						</div>
					</div>

					<?php // do_action( 'grve_header_options_item_container' ); ?>

					<?php //JT grve_print_header_social(); ?>

					<?php // grve_print_header_search(); ?>

					<?php //grve_print_header_shop(); ?>

					<?php // grve_print_header_form(); ?>

					<?php //grve_print_header_language_selector(); ?>

					<?php /* JT 15.11.17: new custom searchbar */ ?>
					<div id="searchbar">
					<?php
						$search = get_search_form();
						echo $search
					?>
					</div>

				</header>



				<?php
					if ( 'vertical' == grve_option( 'menu_style', 'vertical' ) ) {
						if ( '1' == grve_option( 'menu_visibility', '1' ) ) {
				?>

				<nav class="grve-vertical-menu">
					<div class="grve-close-btn"></div>
					<?php grve_header_nav(); ?>
				</nav>

				<?php
					} else {
						do_action( 'grve_header_vertical_navigation_menu' );
					}
					} else {
				?>

				<?php get_template_part('template-parts/progressbar'); ?>

				<?php
					if( is_single() ){
						$value = get_field( "einstiegsbild_verstecken");
						if ( has_post_thumbnail() && !$value ) {
							echo('<section id="jt-postthumb">');
						    the_post_thumbnail('large');
						    echo('</section>');
						}
					}
				?>

				<?php grve_print_header_feature(); ?>
				<?php // grve_print_horizontal_header_title(); ?>

				<?php
					}
				do_action( 'grve_header_after_header' );
				?>

