<?php
/*
 * Template name: Autorenübersicht
 */
?>

<?php get_header(); ?>

  <section class="grve-section">

    <div class="sk-index-container">

      <nav class="sk-indexnav">
        <div class="grve-container">
          <ul>
            <li class="sk-indexnav__item"><a href="#A">A</a></li>
            <li class="sk-indexnav__item"><a href="#B">B</a></li>
            <li class="sk-indexnav__item"><a href="#C">C</a></li>
            <li class="sk-indexnav__item"><a href="#D">D</a></li>
            <li class="sk-indexnav__item"><a href="#E">E</a></li>
            <li class="sk-indexnav__item"><a href="#F">F</a></li>
            <li class="sk-indexnav__item"><a href="#G">G</a></li>
            <li class="sk-indexnav__item"><a href="#H">H</a></li>
            <li class="sk-indexnav__item"><a href="#I">I</a></li>
            <li class="sk-indexnav__item"><a href="#J">J</a></li>
            <li class="sk-indexnav__item"><a href="#K">K</a></li>
            <li class="sk-indexnav__item"><a href="#L">L</a></li>
            <li class="sk-indexnav__item"><a href="#M">M</a></li>
            <li class="sk-indexnav__item"><a href="#N">N</a></li>
            <li class="sk-indexnav__item"><a href="#O">O</a></li>
            <li class="sk-indexnav__item"><a href="#P">P</a></li>
            <li class="sk-indexnav__item"><a href="#Q">Q</a></li>
            <li class="sk-indexnav__item"><a href="#R">R</a></li>
            <li class="sk-indexnav__item"><a href="#S">S</a></li>
            <li class="sk-indexnav__item"><a href="#T">T</a></li>
            <li class="sk-indexnav__item"><a href="#U">U</a></li>
            <li class="sk-indexnav__item"><a href="#V">V</a></li>
            <li class="sk-indexnav__item"><a href="#W">W</a></li>
            <li class="sk-indexnav__item"><a href="#X">X</a></li>
            <li class="sk-indexnav__item"><a href="#Y">Y</a></li>
            <li class="sk-indexnav__item"><a href="#Z">Z</a></li>
          </ul>
        </div>
      </nav>
    </div>

    <div class="grve-container">

      <div id="page-<?php the_ID(); ?>" <?php post_class('grve-main-content'); ?>>

        <?php

          $term_args = array(
            'orderby' => 'display_name',
            'hide_empty' => 0,
            'number' => 5000
          );

          global $coauthors_plus;
          // var_dump($coauthors_plus->coauthor_taxonomy);
          $coauthoer_post_type = $coauthors_plus->guest_authors->post_type;

          $author_terms = get_terms( $coauthors_plus->coauthor_taxonomy, $term_args );
          $authors = array();
          $authors_ordered = array();

          foreach( $author_terms as $author_term ) {
            // Something's wrong in the state of Denmark
            if ( false === ( $coauthor = $coauthors_plus->get_coauthor_by( 'user_login', $author_term->name ) ) )
              continue;

              // JT 24.11.17: strange php warning because data not exists, trying to comment out (!$coauthor->data) {
              if ($coauthor) {
                $authors[$author_term->name] = $coauthor;
                $authors[$author_term->name]->post_count = $author_term->count;

                if (empty($coauthor->last_name))
                  $authors_ordered[] = $coauthor->display_name;
                else
                $authors_ordered[] = $coauthor->last_name;
            }
          }

          /** [jt 23.03.2016 ]
           * enhanced sort function to handle "von XYZ" names
           * old: sort($authors_ordered);
           */
          sort($authors_ordered, SORT_NATURAL | SORT_FLAG_CASE);
          usort($authors, 'comp');
          // var_dump($authors);
          function comp($a, $b) {
            return strcmp($a->last_name, $b->last_name);
          }
          // filter duplicates
          $input = $authors_ordered;
          $authors_ordered = array_unique($input);
        ?>



        <div class="sk-authorindex">

          <?php
          $oldchar = NULL;
          $char = NULL;
          $i = 0;
          foreach ($authors_ordered as $ordered_author): ?>
            <?php foreach ((array)$authors as $author): ?>
            <?php if ($author->post_count && $author->last_name == $ordered_author): ?>

              <?php
                // JT Create alphabetic sections based on first char
                $char = substr($author->last_name, 0, 1);
                if ($oldchar != $char):
                  if ($i > 0){
                    echo '</ul></section>';
                  }
                  $oldchar = $char;
                  $i = 1;
                  ?>
                  <section class="sk-authorindex__section">
                    <a name="<?php echo $char; ?>" class="sk-authorindex_anchor"></a>
                    <h2 class="sk-authorindex_heading"><?php echo $char; ?></h2>
                    <ul>
              <?php else: ?>
              <?php endif; ?>
              <li>
              <article class="sk-authoritem">
                <div class="sk-authoritem__image">
                  <a href="<?php bloginfo('url')?>/autor/<?php echo $author->user_login?>" target="_self" class="">
                    <?php echo get_the_post_thumbnail( $author->ID, 'guest-author-128' ); ?>
                  </a>
                </div>
                <div class="sk-authoritem__title">
                  <a href="<?php bloginfo('url')?>/autor/<?php echo $author->user_login?>">
                    <h3>
                      <?php if (!empty($author->first_name) && !empty($author->last_name)): ?>
                        <span class="author-first-name"><?php echo $author->first_name?></span>
                        <span class="author-last-name"><?php echo $author->last_name?></span>
                      <?php else: ?>
                        <?php echo $author->display_name?>
                      <?php endif;?>
                    </h3>
                  </a>
                </div>
              </article>
            </li>


            <?php endif;?>
            <?php endforeach;?>
          <?php endforeach;?>
        </div>
      </div>

    </div>

  </section>
  <?php get_footer(); ?>

<?php

?>