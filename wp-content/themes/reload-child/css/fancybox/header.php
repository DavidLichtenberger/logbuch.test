<!doctype html>
<html class="no-js grve-responsive" <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title><?php bloginfo('name'); ?> <?php wp_title('|'); ?></title>

		<!-- viewport -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

		<!-- allow pinned sites -->
		<meta name="application-name" content="<?php bloginfo('name'); ?>" />


		<?php
		$grve_favicon = grve_option('favicon','','url');
		if ( ! empty( $grve_favicon ) ) {
		?>
		<link href="<?php echo $grve_favicon; ?>" rel="icon" type="image/x-icon">
		<?php
		}
		?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<link rel="stylesheet" href="<?php echo dirname( get_bloginfo('stylesheet_url')); ?>/css/fancybox/fancybox-1.3.4.css" />
		
		<?php wp_head(); ?>

		<?php 
			wp_register_script('jquery-fancybox', (dirname( get_bloginfo('stylesheet_url')) ."/js/jquery.fancybox-1.3.4.js"));
 			wp_enqueue_script('jquery-fancybox');
 		?>
	</head>

	<body id="grve-body" <?php body_class(); ?>>

		<div id="grve-wrapper">

			<?php
				$grve_icon_color = grve_option( 'header_icon_color' );
				$grve_icon_color_class = 'grve-header-light';
				if ( 'dark' == $grve_icon_color ) {
					$grve_icon_color_class = 'grve-header-dark';
				}

				$grve_header_style_class = "grve-style-1" . " " . $grve_icon_color_class;
				if ( grve_option( 'menu_style', 'vertical' ) == 'horizontal' ) {
					$grve_header_style_class = "grve-style-2" . " " . $grve_icon_color_class;
				} else {
					grve_print_header_feature();
				}
			?>

			<div id="grve-theme-body">


				<?php grve_print_header_item_navigation(); ?>

				<header id="grve-header" class="<?php echo $grve_header_style_class; ?>" data-height="<?php echo grve_option('header_height','90'); ?>">
					<div class="grve-container">
					<?php
						do_action( 'grve_header_before_logo' );
						$grve_logo = grve_option( 'logo','','url' );
						if ( !empty( $grve_logo ) ) {
					?>
						<h1 class="grve-logo">
							<a href="<?php echo home_url(); ?>">
							<?php
								$grve_retina_logo_data = '';
								$grve_retina_logo = grve_option( 'retina_logo','','url' );
								if ( !empty( $grve_retina_logo ) ) {
									$grve_retina_logo_data .= ' data-at2x="' . $grve_retina_logo . '"';
								}
							?>
								<img id="logo-large" src="<?php echo $grve_logo; ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"<?php echo $grve_retina_logo_data; ?>>

							<?php // display small logo ?>						
								<img id="logo-small" 
									src="<?php echo dirname(get_bloginfo('stylesheet_url'))?>/logo-small.png" 
									alt="<?php bloginfo('name'); ?>" 
									title="<?php bloginfo('name'); ?>"
									data-at2x="<?php echo get_template_directory()?>/logo-small@2x.png">


							</a>
						</h1>

						<ul class="header-social-media">
							<li>
								<a href="#" class="fa fa-bars grve-menu-btn"></a>
							</li>
							<li>
								<a class="fa fa-mail" href="/newsletter" target="_blank"></a>
							</li>
							<li>
								<a class="fa fa-facebook" href="https://www.facebook.com/LogbuchSuhrkamp" target="_blank"></a>
							</li>
							<li>
								<a class="fa fa-twitter" href="https://twitter.com/LogbuchSuhrkamp" target="_blank"></a>
							</li>
							<li>
								<a class="fa fa-rss" href="<?php bloginfo('url')?>/feed"></a>
							</li>
							<li>
								<a class="fa fa-search sk-header-search-btn" href="#"></a>
							</li>
						</ul>

					<?php
						}
						do_action( 'grve_header_after_logo' );
					?>

						<?php if ( 'vertical' == grve_option( 'menu_style', 'vertical' ) ) { ?>
						<?php grve_print_vertical_header_title(); ?>
						<?php } ?>

						<?php grve_print_header_options(); ?>

						<?php
						if ( '1' == grve_option( 'menu_visibility', '1' ) ) {
							if ( 'horizontal' == grve_option( 'menu_style', 'vertical' ) ) {
						?>
							<nav class="grve-horizontal-menu">
								<?php grve_header_nav(); ?>
							</nav>
						<?php
							}
						} else {
							do_action( 'grve_header_horizontal_navigation_menu' );
						}
						?>

					</div>

					<?php do_action( 'grve_header_options_item_container' ); ?>

					<?php grve_print_header_social(); ?>

					<?php grve_print_header_search(); ?>

					<?php grve_print_header_shop(); ?>

					<?php grve_print_header_form(); ?>

					<?php grve_print_header_language_selector(); ?>


				</header>

				<?php
					if ( 'vertical' == grve_option( 'menu_style', 'vertical' ) ) {
						if ( '1' == grve_option( 'menu_visibility', '1' ) ) {
				?>

				<nav class="grve-vertical-menu">
					<div class="grve-close-btn"></div>
					<?php grve_header_nav(); ?>
				</nav>

				<?php
						} else {
							do_action( 'grve_header_vertical_navigation_menu' );
						}
					} else {
				?>

				<?php grve_print_header_feature(); ?>
				<?php grve_print_horizontal_header_title(); ?>

				<?php
					}
				?>

