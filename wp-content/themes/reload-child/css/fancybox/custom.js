jQuery(document).ready( function($) {

	var _width = $(window).width();
	var _height = $(window).height();

	$(window).resize(function(e){
		_width = $(window).width();
		_height = $(window).height();
	});

	// responsive logo
	if (_width <= 624) {
		
	}


	$('.sk-about-author-top img, .sk-about-author-top h5').click(function(e){
		e.preventDefault();
		var browserTop = $('body').find('.grve-about-author').offset().top;
		if (browserTop) {
			$('html, body').animate({
				scrollTop: (browserTop -  200)
			}, 500);
		}
	});


	// search form
	$('.header-social-media .sk-header-search-btn').parent().append( $('#grve-search-modal') );

	$(document).on('click', '.sk-header-search-btn', function(e){
		e.preventDefault();
		e.stopPropagation();

		$('#grve-search-modal').toggleClass('active');
	});

	$('.grve-search-btn i').removeClass('fa-search');
	$('.grve-search-btn i').addClass('fa-chevron-circle-right');


	$('p a').click(function(e){
		var href = $(this).attr('href');
		console.log(href.indexOf('.jpg'));

		if (href.indexOf('.jpg') > -1) {
			e.preventDefault();
			$(this).fancybox();
		}
	});

});