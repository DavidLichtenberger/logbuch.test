<?php get_header(); ?>

<?php
global $wp_query, $post;
$current_author = $wp_query->get_queried_object();
$coauthors = get_coauthors($post->ID);
foreach ($coauthors as $coauthor): ?>
	<?php if ($coauthor->user_login == $current_author->user_login): ?>
	<div class="grve-section">
		<div class="grve-container">
			<div class="sk-author-meta">

				<div class="sk-author-image">
					<?php echo get_the_post_thumbnail( $coauthor->ID, 'guest-author-128' ); ?>
				</div>

				<h1><?php echo $coauthor->display_name?></h1>

				<?php $cap_website = get_post_meta($coauthor->ID, 'cap-website', true)?>
				<?php if (!empty($cap_website)): ?>
				<a class="author-link-small" href="<?php echo $cap_website ?>" target="_blank" rel="nofollow">
					<?php // echo $coauthor->display_name?> <?php _e('bei Suhrkamp')?>
				</a>
				<?php endif;?>

				<?php $cap_yahooim = get_post_meta($coauthor->ID, 'cap-yahooim', true)?>
				<?php if (!empty($cap_yahooim)):?>
				<a class="author-link-small" href="<?php echo $cap_yahooim;?>" target="_blank" rel="nofollow">
					<?php _e('Webseite')?> <?php //echo $coauthor->display_name?>
				</a>
				<?php endif;?>

				<p class="sk-author-copyright-info"> Foto: <?php echo get_post_meta($coauthor->ID, 'cap-aim', true); ?></p>

			</div>
			<div class="sk-author-main">

				<div class="sk-author-main__desc"><?php echo do_shortcode( $coauthor->description );?></div>

				<?php /* Posts by author */
				// $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				// $args = array(
				//   'post_type' => 'post',
				//   'posts_per_page' => 1000,
				//   'paged' => $paged,
				//   'post_status' => 'publish',
				//   'author_name' => $current_author->user_login,
				//   //'caller_get_posts' => 1
				// );

				// $author_query = new WP_Query( $args );
				?>
				<?php // if ( $author_query->have_posts() ) : ?>
				<h2 class="sk-author-main__title">Beitr&auml;ge von <?php echo $coauthor->display_name?></h2>

				<?php
				$args = array(
					'author_name' => $current_author->user_login,
					'nopaging' => true
				);
				query_posts( $args );
				require( locate_template( 'template-parts/content-teaserlist.php' ) );
				wp_reset_query();
				?>
				<?php /*<div class="grve-isotope-container isotope grve-blog-masonry" data-columns="2" data-layout="masonry">
					<?php while ( $author_query->have_posts() ) : $author_query->the_post(); setup_postdata($post)?>

					  	<article id="post-<?php the_ID()?>" class="post type-post format-standard hentry category-schreiben-sprechen grve-isotope-item isotope-item">
					  		<div class="grve-post-content">
					  			<a href="<?php echo get_permalink()?>">
					  				<h3 class="grve-post-title"><?php the_title()?></h3>
					  			</a>
					  			<?php the_excerpt()?>
					  		</div>
					  	</article>

					<?php endwhile; ?>
				</div>	*/?>
				<?php //endif;?>

			</div>
		</div>
	</div>
<?php endif ?>
<?php endforeach ?>
<?php get_footer(); ?>