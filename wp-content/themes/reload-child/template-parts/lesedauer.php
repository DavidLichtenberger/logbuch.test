<?php
$lesedauer = get_field('lesedauer');
if ($lesedauer <= 0) $lesedauer = false;
?>
<?php if ($lesedauer) { ?>
  <p class="sk-lesedauer">Lesedauer: <?=$lesedauer;?> min.</p>
<?php } ?>