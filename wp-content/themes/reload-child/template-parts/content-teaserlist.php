<?php /* JT 27.11.17: lazy loading same height row list: Duplicate from index.php	*/ ?>
		<div class="sk-teaser-list">
			<?php
			while ( have_posts() ) : the_post();
				?>
				<article class="sk-teaser">
					<div class="grve-post-content">
						<?php // grve_print_post_feature_media( 'masonry' ); ?>
						<div class="grve-media">
							<a href="<?php the_permalink()?>">
								<?php // grve_print_post_feature_media( 'masonry' ); ?>
								<?php
									$imgid = get_post_thumbnail_id( );
								    $img_src = wp_get_attachment_image_url( $imgid, 'thumbnail' );
								    $img_srcset = wp_get_attachment_image_srcset( $imgid, 'thumbnail' );
								    $image_alt = get_post_meta( $imgid, '_wp_attachment_image_alt', true);
							    ?>
							    	<img src="<?php echo esc_url( $img_src ); ?>" class="sk-teaser-img" srcset="<?php echo esc_attr( $img_srcset ); ?>" sizes="(max-width: 400px) 630px, (max-width: 600px) 630px, (min-width:620px)and(max-width:768px) 630px, (min-width:767px) 630px" alt="<?php echo $image_alt; ?>">
								<?php  ?>
							</a>
						</div>
						<a href="<?php the_permalink()?>">
							<h2><?php the_title()?></h2>
						</a>
						<?php
						// shorten if neccessary
						$excerpt = apply_filters( 'the_excerpt', get_the_excerpt($post->ID));
						if (strlen($excerpt) > 250) {
							$excerpt = substr($excerpt, 0, 250) . ' ...';
						} ?>
						<?php echo $excerpt;?>

						<?php if( function_exists( 'get_coauthors' ) ): ?>
						 	<?php $coauthors = get_coauthors($post->ID);?>
						<?php endif;?>
						<div class="grve-post-meta">
							<div class="grve-post-author">
								<?php echo __( 'von', GRVE_THEME_TRANSLATE ) . '  '; ?>
								<?php $i = 0; ?>
								<?php foreach( $coauthors as $coauthor ): ?>
									<?php if ($i > 0): ?>
										<?php echo ' & '?>
									<?php endif;?>
									<?php $i++; ?>
									<?php echo $coauthor->display_name . ' '?>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</article>
				<?php
			endwhile;
			?>
			<?php /* <!-- fillers for keeping the grid --> */
				if ( is_front_page() || is_home() || is_search() ) {
				  // Default homepage
				} else { ?>

				<div class="sk-teaser sk-teaser-filler"></div>
				<div class="sk-teaser sk-teaser-filler"></div>
				<div class="sk-teaser sk-teaser-filler"></div>

			<?php } ?>


		</div><!-- end list -->
		<?php
			if (!isset($pagination) or $pagination) {
				grve_pagination();
			}
		?>