<?php get_header(); ?>

<div class="grve-section sk-section-author-info">
	<div class="grve-container <?php echo grve_sidebar_class(); ?>">
		<div class="grve-main-content">

			<div class="sk-post-meta-bar-upper">
				<?php grve_print_blog_meta_bar(); ?>
			</div>

			<?php if ( grve_visibility( 'post_author_visibility' ) ) { ?>
				<div class="grve-container sk-about-author-top">
					<?php get_template_part( 'author', 'info-top' ); ?>
				</div>
			<?php } ?>

			<h1><?php the_title(); ?></h1>

			<?php get_template_part( 'template-parts/lesedauer'); ?>

			<?php the_post(); ?>

			<?php get_template_part( 'content', get_post_format() ); ?>

			<div class="sk-post-meta-bar-lower">
				<?php grve_print_blog_meta_bar(); ?>
			</div>

			<?php wp_link_pages(); ?>

			<div class="sk-tags">
				<h3>Schlagwörter</h3>
				<ul>
					<?php the_tags('<li>','','</li>'); ?>
				</ul>
			</div>

			<?php if ( grve_visibility( 'post_author_visibility' ) ) { ?>
				<div class="grve-about-author">
					<?php get_template_part( 'author', 'info' ); ?>
				</div>
			<?php } ?>

			<?php get_template_part( 'bildrechte'); ?>

		</div>
	</div>

	<div id="newsletter">
		<?php
		$nlid = 15007;
		$post_nlid = get_post($nlid);
		$content = $post_nlid->post_content;
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);
		echo $content;
		?>
	</div>

	<?php if ( grve_visibility( 'post_related_visibility' ) ) { ?>
		<!-- Related Posts -->
		<?php sk_grve_print_related_posts(); ?>
		<?php //grve_print_related_posts_2(); ?>
		<!-- End Related Posts -->
	<?php } ?>

	<?php if ( grve_visibility( 'blog_comments_visibility' ) ) { ?>
		<!-- Gap -->
		<div class="grve-gap"></div>
		<!-- End Gap -->
		<?php comments_template(); ?>
	<?php } ?>

	<!-- End Content -->
	<?php get_sidebar(); ?>

	<input type="hidden" id="post-category" value="<?php $cat = get_the_category(); echo $cat[0]->name?>" />

</div>
<?php get_footer(); ?>