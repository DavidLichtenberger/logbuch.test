<?php



/**
 * Dequeue the Parent Main.js and create own
 *
 * Hooked to the wp_print_scripts action, with a late priority (100),
 * so that it is after the script was enqueued.
 */
function sk_dequeue_script() {
	wp_dequeue_script( 'grve-main-script' );
	wp_deregister_script( 'grve-main-script' );
}
add_action( 'wp_print_scripts', 'sk_dequeue_script', 100 );
/**/
function sk_modified_script() {
	wp_register_script('grve-main-script-new', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'),'', true );
	wp_enqueue_script( 'grve-main-script-new' );
}
add_action( 'wp_enqueue_scripts', 'sk_modified_script' );





/**
 * Child Theme's textdomain.
 *
 * Theme Translations can be overwritten in the /languages/ directory.
 */
function grve_child_theme_setup() {
	load_child_theme_textdomain( 'reload', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'grve_child_theme_setup' );


// add custom javascript
function custom_script() {
	wp_register_script('custom', get_stylesheet_directory_uri() . '/custom.js', array('jquery'),'', true );
	wp_enqueue_script( 'custom' );
}
add_action( 'wp_enqueue_scripts', 'custom_script' );

function sk_grve_print_post_categories( $grve_blog_style = 'large-media' ) {
	global $post;
	$post_id = $post->ID;

?>
	<span class="grve-post-author-by">
		<?php echo __( 'by', GRVE_THEME_TRANSLATE ) . ' '; ?><?php the_author_posts_link(); ?>
	</span>
<?php
}



function sk_grve_print_related_posts() {
	global $coauthors_plus;

	// DL 19-02-26 NO CATEGORY FILTER FOR NOW.

	// $grve_cat_ids = array();
	// $grve_cat_slugs = array();
	// $grve_cats_delimited = '';
	// $grve_cat_slugs_delimited = '';

	// $grve_category_list = get_the_category();
	// 	if ( ! empty( $grve_category_list ) ) {
	// 	foreach ( $grve_category_list as $category ) {
	// 		array_push( $grve_cat_ids, $category->cat_ID );
	// 		array_push( $grve_cat_slugs, '.category-' . $category->slug );
	// 	}
	// 	$grve_cats_delimited = implode( ',', $grve_cat_ids );
	// 	$grve_cat_slugs_delimited = implode( ',', $grve_cat_slugs );
	// }

	$grve_tag_ids = array();
	$grve_tag_slugs = array();
	$grve_tags_delimited = '';
	$grve_tag_slugs_delimited = '';

	$grve_tags_list = get_the_tags();
	if ( ! empty( $grve_tags_list ) ) {

		foreach ( $grve_tags_list as $tag ) {
			array_push( $grve_tag_ids, $tag->term_id );
			array_push( $grve_tag_slugs, '.tag-' . $tag->slug );
		}
		$grve_tags_delimited = implode( ',', $grve_tag_ids );
		$grve_tag_slugs_delimited = implode( ',', $grve_tag_slugs );
	}

	global $post;
	$exclude_ids = [ $post->ID ];

	$author = get_the_author_meta('ID');
	if (function_exists('get_coauthors')) {
 		$coauthors = get_coauthors($post->ID);
 		$author = $coauthors[0];
 		$coauthor = $coauthors_plus->get_coauthor_by( 'user_nicename', $author->user_nicename );
 		$term_obj = $coauthors_plus->get_author_term( $coauthor );
 	}

 	$user_info = get_userdata( $author->ID );

 	// var_dump($term_obj);

	$author_found = false;

	$args1 = array(
		'post_type' => 'post',
		'post_status'=>'publish',
		'post__not_in' => $exclude_ids,
		'posts_per_page' => 9,
		'tax_query' => array(
			array(
				'taxonomy' => $coauthors_plus->coauthor_taxonomy,
				'field' => 'slug',
				'terms' => $term_obj->slug
			)
		)
	);

	$query1 = new WP_Query( $args1 );
	if ( $query1->have_posts() ) {
		$author_found = true;
		$exclude_author_ids = array_map(create_function('$p', 'return $p->ID;'), $query1->get_posts() );
		$exclude_ids = array_merge($exclude_ids, $exclude_author_ids);
	}

	// DL 19-02-26 NO CATEGORY FILTER FOR NOW.
	// $lengthAuthors = count($query1->get_posts());
	// $categoryLength = 9 - $lengthAuthors;

	// $cat_found = false;

	// if ($categoryLength > 0) {
	// 	$query2 = array();
	// 	if ( ! empty( $grve_cats_delimited ) ) {
	// 		$args2 = array(
	// 			'cat' => $grve_cats_delimited,
	// 			'post__not_in' => $exclude_ids,
	// 			'posts_per_page' => 9,
	// 			// 'nopaging' => true
	// 		);
	// 		$query2 = new WP_Query( $args2 );
	// 		if ( $query2->have_posts() ) {
	// 			$cat_found = true;
	// 			$exclude_cat_ids = array_map(create_function('$p', 'return $p->ID;'), $query2->get_posts() );
	// 			$exclude_ids = array_merge($exclude_ids, $exclude_cat_ids);
	// 		}
	// 	}
	// }

	$lengthTags = 9 - $lengthAuthors; // - $categoryLength; -> // DL 19-02-26 NO CATEGORY FILTER FOR NOW.
	$tag_found = false;

	// var_dump($grve_tags_delimited);
	// var_dump($exclude_ids);

	//if ($lengthTags > 0) {
		$query3 = array();
		if ( ! empty( $grve_tags_delimited ) ) {
			$args3 = array(
				'tag__in' => $grve_tags_delimited,
				'posts_per_page' => 9,
				// 'nopaging' => true, // DL 19-02-26: ich nehm an, das sollte für einen infinite loader sein? -> lädt ALLE posts!
				'post__not_in' => $exclude_ids
			);
			$query3 = new WP_Query( $args3 );
			if ( $query3->have_posts() ) {
				$tag_found = true;
			}
		}
	//}

	if ( $author_found || $tag_found ) { // || $cat_found -> DL 19-02-26 NO CATEGORY FILTER FOR NOW.
?>

	<div class="grve-related-post grve-blog-label">

		<!-- Related Post Filter -->
		<div class="grve-filter">
			<h5 class="grve-filter-title"><?php _e( 'Related Posts', GRVE_THEME_TRANSLATE ); ?></h5>
			<ul>
				<?php if ( $author_found ) { ?>
				<li data-filter=".grve-filter-author"><i class="fa fa-user"></i><?php _e( 'By Author', GRVE_THEME_TRANSLATE ); ?></li>
				<?php } ?>
				<?php // DL 19-02-26 NO CATEGORY FILTER FOR NOW.
				/* if ( ! empty( $grve_cats_delimited ) ) { ?>
				<li data-filter="<?php echo $grve_cat_slugs_delimited; ?>" class="selected"><i class="fa fa-folder-open"></i><?php _e( 'By Category', GRVE_THEME_TRANSLATE ); ?></li>
				<?php } */?>
				<?php if ( ! empty( $grve_tags_delimited ) ) { ?>
				<li data-filter="<?php echo $grve_tag_slugs_delimited; ?>"><i class="fa fa-tag"></i><?php _e( 'By Tag', GRVE_THEME_TRANSLATE ); ?></li>
				<?php } ?>
			</ul>
		</div>

		<!-- Isotope -->
		<div id="grve-related-posts-container" class="grve-isotope-container" data-columns="3" data-layout="masonry">

			<?php sk_grve_print_loop_related( $query1, 'grve-filter-author');
			// DL 19-02-26 NO CATEGORY FILTER FOR NOW.
			// if ($cat_found) sk_grve_print_loop_related( $query2, 'isotope-hidden' );
			if ($tag_found) sk_grve_print_loop_related( $query3, 'isotope-hidden' ); ?>

		</div>

	</div>
<?php
	}
}



function sk_grve_print_loop_related( $query, $filter = ''  ) {
	global $post;
	$count = 0;
	if ( $query->have_posts() ) :
		while ( $query->have_posts() ) :
			$query->the_post();
			$count++;

			$grve_link = get_permalink();
			$grve_target = '_self';

			if ( 'link' == get_post_format() ) {
				$grve_link = get_post_meta( get_the_ID(), 'grve_post_link_url', true );
				$new_window = get_post_meta( get_the_ID(), 'grve_post_link_new_window', true );

				if( empty( $grve_link ) ) {
					$grve_link = get_permalink();
				}

				if( !empty( $new_window ) ) {
					$grve_target = '_blank';
				}
			} ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class( 'grve-isotope-item ' . $filter ); ?>>
				<a href="<?php echo esc_url( $grve_link ); ?>" target="<?php echo $grve_target; ?>" class="grve-post-wraper">

					<?php $authorname = __( 'von', GRVE_THEME_TRANSLATE ) . '  ';
					if (function_exists('get_coauthors')) {
						$coauthors = get_coauthors($post->ID);
						$i = 0;
						foreach( $coauthors as $coauthor ) {
							if ($i > 0)
								$authorname .= ' & ';

							$authorname .= $coauthor->display_name;
							$i++;
						}
					} ?>

					<div class="sk-author-image">
						<?php the_post_thumbnail( 'grve-image-small-rect' ); ?>
					</div>

					<!-- <div class="grve-post-format"></div> -->

					<div class="grve-post-content">
						<?php //grve_print_post_date(); ?>
						<div class="grve-post-title"><?php the_title(); ?></div>
						<?php if ($authorname): ?>
							<span class="grve-post-date"><?php echo $authorname?></span>
						<?php else:?>
							<span class="grve-post-date"><?php the_author()?></span>
						<?php endif;?>
					</div>
				</a>
			</article>

		<?php endwhile;

	endif;
	wp_reset_postdata();

}


/*
* JT 24.11.17
* Function throws errors, is this needed?
* deactivated

add_filter( 'aioseop_title', 'custom_author_title');
function custom_author_title( $title ) {

	//global $post;

	if (function_exists('get_coauthors')) {
 		$coauthors = get_coauthors($post->ID);
	}

	if ($coauthors) {
		$postauthor = $post_data['post_author'];
		$postAuthorName = $coauthors[0]->display_name;

		global $wp_query, $post;
		$current_author = $wp_query->get_queried_object();
		foreach ($coauthors as $coauthor) {
			if ($coauthor->user_login == $current_author->user_login) {
				$postAuthorName = $coauthor->display_name;
			}
		}

		if( $postAuthorName ) {
			$title = str_replace('%post_author%', $postAuthorName, $title);
		}
	}

	return $title;
}
*/

function filter_search($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}
add_filter('pre_get_posts','filter_search');



/*
JT_ maybe not needed?
function filter_post_link( $permalink, $post ) {

    // Check if the %author% tag is present in the url:
    if ( false === strpos( $permalink, '%author%' ) )
      return $permalink;

    // try to find coauthor name if not return permalink as is
    global $post;
    global $coauthors_plus;

		if (function_exists('get_coauthors')) {
	 		$coauthors = get_coauthors($post->ID);
	 		$author = $coauthors[0];
	 	}

    $author_name = $author->user_login;

    if (!$author_name || !is_single())
    	return $permalink;

    $permalink = str_replace( '%author%', $author_name, $permalink );
    flush_rewrite_rules();

    return $permalink;
}
add_filter( 'post_link', 'filter_post_link' , 10, 2 );
add_filter( 'post_type_link', 'filter_post_link' , 10, 2 );
*/


/*
JT_ maybe not needed?

function add_rewrite_rules() {
	global $wp_query, $wp_rewrite;
	$request = $_SERVER['REQUEST_URI'];
	if (!stristr($request, 'autor/') && !$wp_query->is_404) {

		// try to find coauthor
		$explode = explode('/', $request);
		if ( isset($explode[1]) && isset($explode[2]) ) {
			$coauthors = get_posts(array('post_type' => 'guest-author', 'posts_per_page' => -1));

			foreach ($coauthors as $coauthor) {
				// var_dump($coauthor);
				if ( stristr($coauthor->post_name, $explode[1])) {
					add_rewrite_tag( '%author%', '([^/]+)' );
					$wp_rewrite->flush_rules();
					break;
				}
			}
		}

		//add_rewrite_tag( '%author%', '([^/]+)' );
		//$wp_rewrite->flush_rules();
	}
}
add_action( 'init', 'add_rewrite_rules' );
*/


//add_action( 'template_redirect', 'redirect_primary_coauthor' );

function redirect_primary_coauthor() {
	if ( !is_singular() || !is_single() )
		return;

	$queried = get_query_var( 'coauthor' );

	if ( empty($queried) )
		return;

	$ID = get_the_ID();

	$coauthor = get_coauthor( $ID );

	if ( $coauthor->user_login === $queried ) return;
	$permalink = get_permalink( $ID );
	wp_safe_redirect( $permalink, 301 );
	exit();
}

function get_coauthor( $ID ) {
	$coauthors = get_coauthors( $ID );
	if ( empty( $coauthors ) )
		return;
	$coauthor = current( $coauthors );
	return $coauthor;
}


$request = $_SERVER['REQUEST_URI'];
//var_dump(stristr($request, 'autor/'));

//if (!stristr($request, 'autor/')) {
	//add_action( 'parse_request', 'manipulate_request' );
//}
/*
function manipulate_request( $wp ) {
	$coauthor = isset( $wp->query_vars['author'] ) ? $wp->query_vars['author'] : false;
	//var_dump($wp->request);

	// check if we are not in a author single page

	//var_dump($coauthor);
	//var_dump(is_single());
	//var_dump(!isset($wp->query_vars['name']));
	//var_dump($wp->query_vars['name'] != 'page');
	//exit;

	// we accept only post pages
	if ( $coauthor && isset($wp->query_vars['name']) && $wp->query_vars['name'] != 'page') {

		// try to find a matching coauthor
		if (function_exists('get_coauthors')) {

			global $post;
    	global $coauthors_plus;

	 		$coauthors = get_coauthors($post->ID);
	 		$coauthor = $coauthors[0];

	 		$wp->query_vars['author_name'] = $coauthor->display_name;
			$wp->matched_query = 'author_name=' . $coauthor->display_name;
	 	}
	}

	else {
		unset( $wp->query_vars['author'] );
	}

	return $wp;
}
*/

/* */
function cached_get_page_by_path( $page_path, $output = OBJECT, $post_type = 'page' ) {
	if ( is_array( $post_type ) )
		$cache_key = sanitize_key( $page_path ) . '_' . md5( serialize( $post_type ) );
	else
		$cache_key = $post_type . '_' . sanitize_key( $page_path );
	$page_id = wp_cache_get( $cache_key, 'get_page_by_path' );
	if ( $page_id === false ) {
		$page = get_page_by_path( $page_path, $output, $post_type );
		$page_id = $page ? $page->ID : 0;
		wp_cache_set( $cache_key, $page_id, 'get_page_by_path' ); // We only store the ID to keep our footprint small
	}
	if ( $page_id )
		return get_page( $page_id, $output );
	return null;
}


/* */
function re_rewrite_rules() {
  global $wp_rewrite;

  $wp_rewrite->author_base  = 'autor';
  $wp_rewrite->search_base = 'suchen';
  $wp_rewrite->comments_base = 'kommentare';
  $wp_rewrite->pagination_base = 'seite';
  $wp_rewrite->flush_rules();
}
add_action('init', 're_rewrite_rules');



function copyright_img_caption_shortcode($attr, $content = null) {

	// New-style shortcode with the caption inside the shortcode with the link and image tags.
	if ( ! isset( $attr['caption'] ) ) {
		if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
			$content = $matches[1];
			$attr['caption'] = trim( $matches[2] );
		}
	}

	// Allow plugins/themes to override the default caption template.
	$output = apply_filters('img_caption_shortcode', '', $attr, $content);
	if ( $output != '' )
		return $output;

	$atts = shortcode_atts( array(
		'id'	  => '',
		'align'	  => 'alignnone',
		'width'	  => '',
		'caption' => ''
	), $attr, 'caption' );

	$atts['width'] = (int) $atts['width'];
	if ( $atts['width'] < 1 || empty( $atts['caption'] ) )
		return $content;

	if ( ! empty( $atts['id'] ) )
		$atts['id'] = 'id="' . esc_attr( $atts['id'] ) . '" ';

	$caption_width = 10 + $atts['width'];
	$caption_width = apply_filters( 'img_caption_shortcode_width', $caption_width, $atts, $content );

	$style = '';
	if ( $caption_width )
		$style = 'style="width: ' . (int) $caption_width . 'px" ';

	// also get the image copyright info
	// $d = wp_get_attachment_metadata( str_replace('attachment_', '', $attr['id']) );
	// $copyright = get_post_meta(str_replace('attachment_', '', $attr['id']), '_wp_attachment_image_alt', true);
	// $copyrightHtml = '<span class="copyright grve-post-date">' . $copyright . '</span>';

	return '<div ' . $atts['id'] . $style . 'class="wp-caption ' . esc_attr( $atts['align'] ) . '">'
	. do_shortcode( $content ) . '<p class="wp-caption-text">' . $atts['caption'] . '</p></div>';
}

add_shortcode('wp_caption', 'copyright_img_caption_shortcode');
add_shortcode('caption', 'copyright_img_caption_shortcode');




/** [jt 23.03.2016 ]
 * Co-authors in RSS and other feeds
 * /wp-includes/feed-rss2.php uses the_author(), so we selectively filter the_author value
 */
function db_coauthors_in_rss( $the_author ) {

	global $coauthors_plus;
	global $post;

	if ( !is_feed() || !function_exists('get_coauthors') ){
		return $the_author;
	} else {
		$coauthors = get_coauthors($post->ID);
		$author = $coauthors[0];
		$coauthor = $coauthors_plus->get_coauthor_by( 'user_nicename', $author->user_nicename );
		return $coauthor->display_name;
	}
}
add_filter( 'the_author', 'db_coauthors_in_rss' );


/** [jt 23.03.2016 ]
 * add post thumbnail to feed
 */
function add_feed_content($content) {

	if(is_feed()) {
		global $post;

		// jt 02.12.2020: only use first paragraph wit the headline, then add the pic
		$start = strpos($content, '<p>');
		$end = strpos($content, '</p>', $start);
		$paragraph = substr($content, $start, $end-$start+4);
		//$content = '<a href="' . get_the_permalink( $post ) . '">';
		$content = strip_tags( $paragraph );
		//$content .= '</a>';
		
		if(has_post_thumbnail($post->ID)) {
			$thumbnail_id = get_post_thumbnail_id($post->ID);
			$thumbnail_url = wp_get_attachment_image_src($thumbnail_id,'thumbnail', true);
			$content .= '<p><img src="' . $thumbnail_url[0] . '" style="width: 100%; max-width: 100%;" /></p>';
		}
	}
	return $content;
}
add_filter('the_excerpt_rss', 'add_feed_content');
//add_filter('the_content', 'add_feed_content');


/** [jt 28.04.2017 ]
 * add co author to title
  */
function add_feed_title($content) {

	global $coauthors_plus;
	global $post;
	global $wp_query;

	if ( !is_feed() || !function_exists('get_coauthors') ){
		return $content;
	} else {
		$coauthors = get_coauthors($post->ID);
		$author = $coauthors[0];
		$coauthor = $coauthors_plus->get_coauthor_by( 'user_nicename', $author->user_nicename );
		$coauthor = $coauthor->display_name;
		$content = $content .' von '. $coauthor;
		return $content;
	}

}
add_filter('the_title_rss', 'add_feed_title');



/** [jt 28.04.2017 ]
 * disable comment feed
  */
add_filter( 'feed_links_show_comments_feed', '__return_false' );


// function custom_feed_title($title) {

//     $headline = 'asdsd';

//     if (!empty($headline))
//         return $headline;

//     return $title;
// }
// add_filter('the_title_rss', 'custom_feed_title');




/** [jt 23.03.2016 ]
 * Extend WordPress search to include custom fields
 *
 * http://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );


/*
/** [jt 05.04.2016 ]
* change permalinks for posts to have the custom field "coauthor" inside the post permalink
* tag should exist in on permalinks page: %coauthor%
*/

// Add rewrite tag
function jt_register_rewrite_tag() {
    add_rewrite_tag( '%coauthor%', '([^/]+)');
}
add_action( 'init', 'jt_register_rewrite_tag');


// Add rewrite rules | prevents a paged site to be identified as post through the above rewrite tag
function jt_rewrite_rule() {
	add_rewrite_rule("(.?.+?)/seite/?([0-9]{1,})/?$",'index.php?page_name=$matches[1]&paged=$matches[2]','top');
}
add_action('init', 'jt_rewrite_rule', 10, 0);


// filter post link. needed to replace %coauthor% value
function jt_filter_post_link( $permalink, $post ) {

    // Check if the %coauthor% tag is present in the url:
    if ( false === strpos( $permalink, '%coauthor%' ) )
        return $permalink;

    // Get the coauthors value in post meta
	global $coauthors_plus;
	if (function_exists('get_coauthors')) {
 		$coauthors = get_coauthors($post->ID);
 		$author = $coauthors[0];
 		$author_name = $coauthors[0]->user_login;

	    // Unfortunately, if no date is found, we need to provide a 'default value'.
	    $author_name = ( ! empty($author_name) ? $author_name : '' );
	    $author_name = urlencode($author_name);

	    // Replace '%coauthor%'
	    $permalink = str_replace( '%coauthor%', $author_name , $permalink );
	    return $permalink;
	}

    //flush_rewrite_rules();
}
add_filter( 'post_link', 'jt_filter_post_link' , 10, 2 );


/** [jt 18.05.2016 ]
* add coauthor to all in one seo title
* extends all in one seo plugin
*/
add_filter('aioseop_title', 'sfwd_custom_title');
function sfwd_custom_title($title) {
	global $coauthors_plus;
   	if ( is_single() && function_exists('get_coauthors')) {

 		$coauthors = get_coauthors($post->ID);
 		$author_name = $coauthors[0]->display_name;
 		if($author_name != "" ){
 			$title = $author_name . ': ' . $title;
 		}

   }
   return $title;
}


/*
/** [jt 13.04.2016 ]
* custom field for switch to not show the article leadimage
* dependent on ACF plugin
*/
define( 'ACF_LITE', false );
//include_once('advanced-custom-fields/acf.php');

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_einstiegsbild',
		'title' => 'Einstiegsbild',
		'fields' => array (
			array (
				'key' => 'field_570e94d7b5804',
				'label' => 'Einstiegsbild verstecken',
				'name' => 'einstiegsbild_verstecken',
				'type' => 'true_false',
				'instructions' => 'Auf der Artikelvollansicht:',
				'message' => '',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 99,
	));
}

if( function_exists('acf_add_local_field_group') ) {

	acf_add_local_field_group(array(
		'key' => 'group_5ae98567f3439',
		'title' => 'Fussnoten',
		'fields' => array(
			array(
				'key' => 'field_5ae9859df9cce',
				'label' => 'Fussnoten Feld',
				'name' => 'fussnoten_feld',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

	/* LESEDAUER */
	acf_add_local_field_group(array(
		'key' => 'group_5e4a9b0d8ee67',
		'title' => 'Lesedauer',
		'fields' => array(
			array(
				'key' => 'field_5e4a9b1025f48',
				'label' => 'Lesedauer',
				'name' => 'lesedauer',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => 'min.',
				'min' => '',
				'max' => '',
				'step' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'side',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
}

/*
/** [jt 15.02.2017 ]
* add custom css to admin
*
*/
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    .attachment-details label[data-setting=caption] span::before {
    	content: "BU / ";
    }
    .attachment-details label[data-setting=description] span::before {
    	content: "Copyright / ";
    }
  </style>';
}

/*
/** [jt 17.02.2017 ]
* all search results
*
*/
function change_wp_search_size($query) {
    if ( $query->is_search ) // Make sure it is a search page
        $query->query_vars['posts_per_page'] = -1; // Change 10 to the number of posts you would like to show

    return $query; // Return our modified query variables
}
add_filter('pre_get_posts', 'change_wp_search_size'); // Hook our custom function onto the request filter


/*
** [jt 08.03.2017 ]
* custom js
*/
add_action( 'wp_enqueue_scripts', 'register_custom_js' );
function register_custom_js() {
	wp_enqueue_script( 'jttheme-waypoints', get_stylesheet_directory_uri() . '/js/jquery.waypoints.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'jttheme-waypoints-infinite', get_stylesheet_directory_uri() . '/js/waypoints.shortcuts/infinite.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'jttheme-waypoints-sticky', get_stylesheet_directory_uri() . '/js/waypoints.shortcuts/sticky.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), '', true );
}






/**
 * [jt 15.11.2017 ]
 * Custom Searchform
 */
/**
 * Generate custom search form
 *
 * @param string $form Form HTML.
 * @return string Modified form HTML.
 */
function sk_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
    <div><label class="screen-reader-text" for="s">' . __( 'Search for:' ) . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="'. esc_attr__( 'Search' ) .'" />
    </div>
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'sk_search_form' );



/**
 * [jt 24.11.2017 ]
 * Remove Dynamic CSS from theme options in order to have clean base for writing the css
 */
add_action( 'after_setup_theme', 'wpdev_170663_remove_parent_theme_stuff', 0 );
function wpdev_170663_remove_parent_theme_stuff() {
    remove_action( 'wp_head', 'grve_load_dynamic_css' );
}



/**
 * [jt 25.10.2017 ]
 * Clean Up Standards: like empty header etc
 *
 *
 * Remove All Meta Generators
*/
function remove_meta_generators($html) {
    $pattern = '/<meta name(.*)=(.*)"generator"(.*)>/i';
    $html = preg_replace($pattern, '', $html);
    return $html;
}
function clean_meta_generators($html) {
    ob_start('remove_meta_generators');
}
add_action('get_header', 'clean_meta_generators', 100);
add_action('wp_footer', function(){ ob_end_flush(); }, 100);


/**
 * Remove the migrate script from the list of jQuery dependencies.
 *
 * @see https://github.com/cedaro/dequeue-jquery-migrate
 *
 * @param WP_Scripts $scripts WP_Scripts scripts object. Passed by reference.
 */
function dequeue_jquery_migrate( $scripts ) {
    if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) {
        $jquery_dependencies = $scripts->registered['jquery']->deps;
        $scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
    }
}
add_action( 'wp_default_scripts', 'dequeue_jquery_migrate' );


/*
* disable oembed
*/
	/**
	 * Disable embeds on init.
	 *
	 * - Removes the needed query vars.
	 * - Disables oEmbed discovery.
	 * - Completely removes the related JavaScript.
	 *
	 * @since 1.0.0
	 */
	function evolution_disable_embeds_init() {
	    /* @var WP $wp */
	    global $wp;

	    // Remove the embed query var.
	    $wp->public_query_vars = array_diff( $wp->public_query_vars, array(
	        'embed',
	    ) );

	    // Remove the REST API endpoint.
	    remove_action( 'rest_api_init', 'wp_oembed_register_route' );

	    // Turn off oEmbed auto discovery.
	    add_filter( 'embed_oembed_discover', '__return_false' );

	    // Don't filter oEmbed results.
	    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

	    // Remove oEmbed discovery links.
	    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

	    // Remove oEmbed-specific JavaScript from the front-end and back-end.
	    remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	    add_filter( 'tiny_mce_plugins', 'evolution_disable_embeds_tiny_mce_plugin' );

	    // Remove all embeds rewrite rules.
	    //add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );

	    // Remove filter of the oEmbed result before any HTTP requests are made.
	    remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
	}

	add_action( 'init', 'evolution_disable_embeds_init', 9999 );

	/**
	 * Removes the 'wpembed' TinyMCE plugin.
	 *
	 * @since 1.0.0
	 *
	 * @param array $plugins List of TinyMCE plugins.
	 * @return array The modified list.
	 */
	function evolution_disable_embeds_tiny_mce_plugin( $plugins ) {
	    return array_diff( $plugins, array( 'wpembed' ) );
	}

	/**
	 * Remove all rewrite rules related to embeds.
	 *
	 * @since 1.0.0
	 *
	 * @param array $rules WordPress rewrite rules.
	 * @return array Rewrite rules without embeds rules.
	 */
	function evolution_disable_embeds_rewrites( $rules ) {
	    foreach ( $rules as $rule => $rewrite ) {
	        if ( false !== strpos( $rewrite, 'embed=true' ) ) {
	            unset( $rules[ $rule ] );
	        }
	    }

	    return $rules;
	}

	/**
	 * Remove embeds rewrite rules on plugin activation.
	 *
	 * @since 1.0.0
	 */
	function evolution_disable_embeds_remove_rewrite_rules() {
	    add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );
	    flush_rewrite_rules( false );
	}

	register_activation_hook( __FILE__, 'evolution_disable_embeds_remove_rewrite_rules' );

	/**
	 * Flush rewrite rules on plugin deactivation.
	 *
	 * @since 1.0.0
	 */
	function evolution_disable_embeds_flush_rewrite_rules() {
	    remove_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );
	    flush_rewrite_rules( false );
	}

	register_deactivation_hook( __FILE__, 'evolution_disable_embeds_flush_rewrite_rules' );



/**
 * Befreit den Header von unnötigen Einträgen
 */
add_action('init', 'evolution_remheadlink');
function evolution_remheadlink()
{
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'wlwmanifest_link');
   // remove_action('wp_head', 'feed_links', 2);
   // remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'wp_shortlink_header', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

    remove_action( 'wp_head', 'rest_output_link_wp_head' );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
    remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
}

/*
* remove emojis https://fastwp.de/4903/
*/
function remove_emoji()
	{
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	add_filter('tiny_mce_plugins', 'remove_tinymce_emoji');
	}
add_action('init', 'remove_emoji');

function remove_tinymce_emoji($plugins) {
if (!is_array($plugins))
	{
	return array();
	}
return array_diff($plugins, array(
	'wpemoji'
));
}



/*
* XML-RPC komplett deaktivieren https://fastwp.de/2068/
*/
add_filter( 'wp_headers', 'FastWP_remove_x_pingback' );
function FastWP_remove_x_pingback( $headers ){
	unset( $headers['X-Pingback'] );
	return $headers;
}




/* Co-Authors in Feeds */
// add_filter( 'the_title', function($content) {
// 	// if ( is_feed() && function_exists('get_coauthors') )  {

// 	// 	global $post;
// 	// 	$content = '';
// 	// 	$authors = get_coauthors($post->ID);

// 	// 	for ($i = 0; $i < count($authors); $i++) {
// 	// 		if ($i != 0) {
// 	// 			$content .= ($i == count($authors) - 1) ? ' & ' : ', ';
// 	// 		}
// 	// 		$content .= $authors[$i]->display_name;
// 	// 	}
// 	// }
// 	// return $content;
// 	// die('title filter' . $content);
// }, 10);


add_filter( 'the_title_rss', function($content) {
	if ( is_feed() && function_exists('get_coauthors') )  {

		global $post;

		$title = get_the_title($post->ID);
		$content = $title . ' ... von ';

		$authors = get_coauthors($post->ID);
		for ($i = 0; $i < count($authors); $i++) {
			if ($i != 0) {
				$content .= ($i == count($authors) - 1) ? ' &amp; ' : ', ';
			}
			$content .= $authors[$i]->display_name;
		}

	}
	return $content;
}, 10);

add_filter( 'the_author', function($content) {
	if ( is_feed() && function_exists('get_coauthors') )  {

		global $post;
		$content = '';

		$authors = get_coauthors($post->ID);
		for ($i = 0; $i < count($authors); $i++) {
			if ($i != 0) {
				$content .= ($i == count($authors) - 1) ? ' &amp; ' : ', ';
			}
			$content .= $authors[$i]->display_name;
		}
	}
	return $content;
}, 10);



add_action( 'wp_head', function () {
  ?>
  <script type="text/javascript">
jQuery( document ).ready(function() {
    //   console.info("cookie");
	let $cookieLink = jQuery('a[href="#cookies"]');
	$cookieLink.addClass('borlabs-cookie-preference');	

});
  </script>
  <?php
}, 10 );
